.. title: Open Source Management and Monitoring System for Ceph
.. slug: index
.. date: 2016-11-19 10:15:28 UTC+01:00
.. tags:
.. category:
.. link:
.. description: openATTIC.org index page
.. type: text
.. pretty_url: False
.. nocomments: True

openATTIC is an Open Source Management and Monitoring System for the `Ceph
distributed storage system <http://ceph.com>`_.

Various resources of a Ceph cluster can be managed and monitored via a web-based
management interface. It is no longer necessary to be intimately familiar with
the inner workings of the individual Ceph components.

Any task can be carried out by either using openATTIC’s clean and intuitive web
interface or via the openATTIC REST API.

openATTIC itself is stateless - it remains in a consistent state even if you
make changes to the Ceph cluster's resources using external command-line tools.

openATTIC is based on a modern and extensible architecture, built with proven
web technologies like `AngularJS <https://angularjs.org/>`_, `Bootstrap
<http://getbootstrap.com/>`_ and the `Django <https://www.djangoproject.com/>`_
web application framework.

As of version 3.x, openATTIC helps managing the following aspects of a Ceph cluster:

* Dashboard: Monitor and visualize the overall health status and key performance
  metrics of the entire cluster
* Ceph Pools: Create, manage and monitor individual pools (both replicated and
  erasure coded pools)
* Ceph Block devices (RBDs): create, manage and monitor RBDs
* iSCSI: manage iSCSI targets and portals, access control
* NFS: create and manage NFS shares (NFSv3 and NFSv4, using CephFS or S3 buckets)
* Ceph Object Gateway (RGW): manage users, access keys, quotas and buckets
* Ceph Node management: list all cluster nodes and their roles, monitor per-node
  key performance metrics

See the :doc:`feature list <features>` for details.

:doc:`Installation packages <download>` are available in the form of package
repositories for Linux.

The project is sponsored by `SUSE <https://suse.com/>`_. It is licensed under
the GNU General Public License (GPLv2) and follows a completely :doc:`open and
inclusive development process <get-involved>`.
