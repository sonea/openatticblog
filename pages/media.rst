.. title: Media
.. slug: media
.. date: 2016-12-05 10:15:28 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: openATTIC.org screen shots and videos
.. type: text
.. pretty_url: False
.. nocomments: True

openATTIC 3.x Screen Shots
--------------------------

See the `openATTIC 3.x screenshot gallery </galleries/oA-3.x-Screenshots/>`_
for full-size images.

.. slides::

    /galleries/oA-3.x-Screenshots/oA-3.x-dashboard.png
    /galleries/oA-3.x-Screenshots/oA-3.x-nodes.png
    /galleries/oA-3.x-Screenshots/oA-3.x-osds.png
    /galleries/oA-3.x-Screenshots/oA-3.x-pools.png
    /galleries/oA-3.x-Screenshots/oA-3.x-rbds.png
    /galleries/oA-3.x-Screenshots/oA-3.x-NFS.png
    /galleries/oA-3.x-Screenshots/oA-3.x-create-NFS-export.png
    /galleries/oA-3.x-Screenshots/oA-3.x-create-iscsi-target.png
    /galleries/oA-3.x-Screenshots/oA-3.x-create-pool.png
    /galleries/oA-3.x-Screenshots/oA-3.x-create-rbd.png
    /galleries/oA-3.x-Screenshots/oA-3.x-create-RGW-add-user.png
    /galleries/oA-3.x-Screenshots/oA-3.x-Settings.png

openATTIC 2.x Screen Shots
--------------------------

See the `openATTIC 2.x screenshot gallery </galleries/oA-2.x-Screenshots/>`_
for full-size images.

.. slides::

    /galleries/oA-2.x-Screenshots/openATTIC-Storage_Dashboard.png
    /galleries/oA-2.x-Screenshots/openATTIC-Volume_Management.png
    /galleries/oA-2.x-Screenshots/openATTIC-API_Recorder.png
    /galleries/oA-2.x-Screenshots/openATTIC-Ceph_CRUSH_Map.png
    /galleries/oA-2.x-Screenshots/openATTIC-Ceph_Dashboard.png
    /galleries/oA-2.x-Screenshots/openATTIC-Ceph_OSD_List.png
    /galleries/oA-2.x-Screenshots/openATTIC-Ceph_Pool_Creation.png
    /galleries/oA-2.x-Screenshots/openATTIC-Ceph_Pool_List.png
    /galleries/oA-2.x-Screenshots/openATTIC-Ceph_RBD_Creation.png
    /galleries/oA-2.x-Screenshots/openATTIC-Ceph_RBD_List.png

Videos
------

You can find videos of presentations about openATTIC as well as screen casts
on the `openATTIC YouTube Channel <https://www.youtube.com/user/openATTICManagement>`_

.. youtube:: Ooh-tjSVmNs