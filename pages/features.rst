.. title: Features
.. slug: features
.. date: 2016-11-19 10:21:31 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
.. pretty_url: False
.. nocomments: True

Ceph Management and Monitoring
------------------------------

openATTIC utilizes various technologies and frameworks to provide an easy to use
web interface for the Ceph management and monitoring capabilities provided in
version 3.x:

* `Prometheus <https://prometheus.io/>`_ and `Grafana <https://grafana.com/>`_
  (for monitoring and visualization), using `Digital Ocean's Ceph Exporter
  for Prometheus <https://github.com/digitalocean/ceph_exporter>`_ for gathering
  the data
* `DeepSea <https://github.com/SUSE/DeepSea>`_, a `Salt-based
  <https://saltstack.com/salt-open-source/>`_ framework for installing,
  configuring and managing a Ceph cluster (via the `Salt REST API
  <https://docs.saltstack.com/en/latest/ref/netapi/all/salt.netapi.rest_cherrypy.html>`_)
* `NFS Ganesha <http://nfs-ganesha.github.io/>`_ (managed via DeepSea) for
  sharing Ceph storage via NFS
* `lrbd <https://github.com/SUSE/lrbd/>`_ (managed via DeepSea) for iSCSI target
  management

Based on these technologies, openATTIC supports managing the following resources
of a Ceph cluster (running Ceph "Luminous"). Check out the `live demo
<http://demo.openattic.org/openattic>`_ and see the `openATTIC 3.x screenshot
gallery </galleries/oA-3.x-Screenshots/>`_ for pictures.

* Dashboard: Monitor and visualize the overall health status and key performance
  metrics of the entire cluster via custom Grafana dashboards
* Ceph Pools: View, create, manage and monitor individual pools (both replicated and
  erasure coded pools, with and without compression)
* Ceph Pool Erasure code profile management
* Ceph Block devices (RBDs): view, create, manage and monitor RBDs
* iSCSI: manage iSCSI targets and portals, access control (requires DeepSea and lrbd)
* NFS: view, create and manage NFSv3 and NFSv4 shares on top of CephFS or S3 buckets
  (requires DeepSea and NFS Ganesha)
* Ceph Object Gateway (RGW): manage users, access keys, quotas and buckets (via the
  `RADOS Gateway Admin Ops API <http://docs.ceph.com/docs/master/radosgw/adminops/>`_)
* Ceph Node management: list all cluster nodes and their roles, monitor per-node
  key performance metrics
* Object Storage Daemon (OSD) management (view)
* CRUSH Map viewer

Other features
--------------

* Multi-user support
* API Recorder for easy application development/debugging

Missing a feature?
------------------

openATTIC is under active development. If you're missing a particular feature,
please :doc:`let us know <get-involved>`!