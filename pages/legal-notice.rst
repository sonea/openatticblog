.. title: Legal Notice
.. slug: legal-notice
.. date: 2018-09-26 13:09:31 UTC+02:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
.. pretty_url: False
.. nocomments: True

| **SUSE LINUX GmbH**
| Maxfeldstr. 5
| 90409 Nürnberg
| Germany

| Registration Number: HRB 21284 AG Nürnberg Managing Director: Graham Norton, Jane Smithard, Felix Imendörffer
| Tax ID: DE 192 167 791 Legal Venue: Nürnberg

| Contact: Felix Imendörffer
| Email: `crc@suse.com <mailto:crc@suse.com>`_
| Tel: +49 (0)911 - 740 53 - 0

Terms of Use
------------
SUSE® ("SUSE", "we" or "our") makes available information, materials, and products on this Website and other related websites from our affiliates and subsidiaries (“Website” or “site”), subject to the following terms. By accessing or continuing to use this site or downloading or otherwise using any of the materials published on or made available for download from this Website, you agree to these Website terms and to the SUSE `privacy </privacy-notice.html>`_ policy which by these references are hereby expressly incorporated into these Website terms. If you do not agree to these terms of use, please refrain from using this Website.

Any rights not expressly granted herein are reserved by SUSE. If any of these terms shall be deemed invalid, void, or for any reason unenforceable, it shall be deemed severable and shall not affect the validity and enforceability of any remaining terms. SUSE reserves the right to change these terms from time to time at its sole discretion and without notice. You are expected to check this page from time to time to take notice of any changes made, as they are binding on you. Some of the provisions contained in these terms of use may also be superseded by provisions or notices published elsewhere on this Website.

Links to Third-Party Websites
-----------------------------
This Website contains links to third-party Websites, which are not under the control of SUSE. SUSE makes no representations whatsoever about any Websites to which you may have access through this Website. When you access a non-SUSE Website, you do so at your own risk and SUSE is not responsible for the nature, quality, accuracy or reliability of any information, data, opinions, advice or statements made on these sites. SUSE expressly disclaims any potential liability associated with any such information and such information is not incorporated by reference into this Website. SUSE provides these links merely as a convenience and the inclusion of such links does not imply that SUSE or its management endorses or accepts any responsibility for the content or uses of such Websites. By clicking on any of the hyperlinks to third-party Websites on this website, you acknowledge and agree to the foregoing limitations.

You agree that you will not arrange for any third party website to be connected to any part of this site by way of hyperlink or otherwise without our prior written consent, and if we grant such consent, then we reserve the right to withdraw such consent at any time in our discretion. You may not use any of our proprietary logos, trademarks, or other distinctive graphics, video, or audio material in your links, and you may not link in any manner reasonably likely to (i) imply affiliation with us or endorsement or sponsorship by us without our express written consent, (ii) cause confusion, mistake, or deception, (iii) dilute our trademarks, service marks, or trade names, or (iv) otherwise breach applicable law.

Use of Website Materials
------------------------
All content, data, ideas, comments, feedback, suggestions, expression of ideas or other materials on this Website including software, code and accompanying documentation (any or all of the foregoing being "Materials") are protected by copyright trade dress and other laws. Except as specifically permitted herein, no Materials on this Website may be reproduced in any form or by any means without express prior written permission from SUSE.

All rights, title and interest not expressly granted are reserved. Use for any purpose other than as expressly authorized by us is expressly prohibited by law.

SUSE does not guarantee continuous or secure access to this Website and the operation of this Website may be interfered with by numerous factors outside of its control. SUSE is not responsible for any loss or damage caused by the temporary interruption of this Website due to faults or circumstances outside of its control including but not limited to link failures, power difficulties, telephone outages, network overload, default or failure of a third party (including a public telecommunications operator), government actions, failure in the supply of a third party's access line or any event of force majeure.

You must not misuse this Website by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorized access to any SUSE site, the server on which our site is stored or any server, computer or database connected to our site. You must not attack our site via a denial-of-service attack or a distributed denial-of-service attack. By breaching this provision, you would commit a criminal offence under applicable laws and we will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our site will cease immediately.

RSS Feeds
---------
SUSE provides any RSS feeds as a free service and for personal, non-commercial use only, and subject to these Website terms. SUSE reserves the right to discontinue any such service at any time and further reserves the right to request the immediate cessation of any specific use of its RSS service.

Warranties and Disclaimers
--------------------------
Except as expressly provided otherwise in an agreement between you and SUSE, all information, materials and software on this website is provided "as is" without any other warranties or conditions, express or implied, including, but not limited to, the warranties of merchantable quality, satisfactory quality, merchantability or fitness for a particular purpose, or those arising by law, statute, usage of trade, or course of dealing.

SUSE assumes no responsibility for errors or omissions in the information, materials or software which are referenced by or linked to this website. References to any other corporations, their services and products are provided "as is" without warranty of any kind, either express or implied.

In no event shall SUSE or any SUSE entities be liable for any special, incidental, indirect or consequential losses or damages of any kind, or any other losses or damages of any nature whatsoever, including, without limitation, those resulting from loss of use, data, business, opportunity or profits, whether or not advised of the possibility of loss or damage, and on any theory of liability, arising out of or in connection with the use or performance of this information. Some states/countries do not allow the exclusion or limitation of liability for consequential or incidental damages, so the above limitation may not apply to you.

This website could include technical or other inaccuracies or typographical errors. Changes are periodically added to the information contained in this website, and these changes will be incorporated in new editions of this website. SUSE may make improvements and/or changes in the product(s) and/or the program(s) described in this website at any time.

This Website can be accessed from other countries around the world and may contain references to SUSE products, services and programs that have not been announced in your country. These references do not imply that SUSE intends to announce such products, services or programs in your country.

Forward–Looking Statements Disclaimer
-------------------------------------
Certain statements contained on this Website may now, or hereafter from time to time, constitute “forward-looking statements”. All statements other than statements of historical facts included on this Website, including, without limitation, those regarding SUSE’s financial condition, business strategy, plans and objectives, are forward-looking statements. These forward-looking statements can be identified by the use of forward-looking terminology, including the terms “believes”, “estimates”, “anticipates”, “expects”, “intends”, “may”, “will” or “should” or, in each case, their negative or other variations or comparable terminology. Such forward-looking statements involve known and unknown risks, uncertainties and other factors, which may cause the actual results, performance or achievements of SUSE, or industry results, to be materially different from any future results, performance or achievements expressed or implied by such forward-looking statements. Such forward-looking statements are based on numerous assumptions regarding SUSE’s present and future business strategies and the environment in which it will operate in the future. Such risks, uncertainties and other factors include, among others: the level of expenditure committed to development and deployment applications by information technology organizations; the degree to which information technology organizations adopt web enabled services; the rate at which large organizations migrate applications from the main frame environment; the continued use and necessity of the main frame for business critical applications; the degree of competition faced by SUSE; growth in the information technology services market, general economic and business conditions in any country in or with which SUSE conducts business, changes in technology, competition and ability to attract and retain personnel.

Except as required by law, SUSE does not undertake any obligation to update or revise publicly any forward-looking statement, whether as a result of new information, future events or otherwise. SUSE expressly disclaims any obligation or undertaking to release publicly any updates or revisions to any forward-looking statement contained herein to reflect any change in the its expectations with regard thereto or any change in events, conditions or circumstances on which any such statement is based.

