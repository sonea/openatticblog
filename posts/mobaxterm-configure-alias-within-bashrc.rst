.. title: MobaXterm - Configure alias within .bashrc
.. slug: mobaxterm-configure-alias-within-bashrc
.. date: 2015-09-17 15:53:41 UTC+02:00
.. tags: howto windows ssh 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

Today i found out, that you can create and use the .bashrc file within mobaxterm.

So just create the .bashrc file and open a new tab - thats it!

Now you can configure stuff like that::

	alias blog='ssh blog@blogserver.domain.example'

Very cool :-)
