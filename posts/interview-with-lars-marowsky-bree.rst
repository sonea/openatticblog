.. title: Interview with Lars Marowsky-Brée, Distinguished Engineer and Architect of SUSE Enterprise Storage
.. slug: interview-with-lars-marowsky-bree
.. date: 2016-02-17 09:53:03 UTC+01:00
.. tags: suse, storage, community, collaboration
.. category:
.. link:
.. description: Q&A session with SUSE's Lars Marowsky-Brée about openATTIC
.. type: text
.. author: Lenz Grimmer

I recently sat down with `Lars Marowsky-Brée <https://twitter.com/larsmb>`_
from SUSE, to discuss the details of our joint collaboration on openATTIC that
has just been :doc:`announced <collaborating-with-suse>`.

In this interview, we talk about Ceph, collaboration on open source projects
and why openATTIC was chosen as an ideal candidate for becoming the management
frontend for SUSE's storage product.

.. TEASER_END

**Lenz: Hi Lars, pleased to meet you! Thank you for letting me to ask you some
questions. Now that the word is finally out, I think it would be good to share
some background information about our plans with the openATTIC community.
To give our readers a little background, could you please introduce yourself?
What's your role at SUSE?**

**Lars:** Hi Lenz, thanks for the chance to provide more details and context
about this exciting new development!

I have been with SUSE for 16 years now; my previous roles (ranging from
consulting to development and architecture) were centered on High Availability
Clustering and distributed systems, in which storage already plays a key role.
All of our work has been and will be on Open Source software, and bringing
this to our Enterprise customers.

Currently, I serve as the architect for Software Defined Storage product at
SUSE. This role exists within our growing software engineering organisation,
and my responsibility is to provide the technical guidance and vision for
building a strong product based on, but not limited to, the Ceph project.

**Lenz: Working on a Ceph-based product sounds pretty exciting, thanks for
this introduction. 16 years is quite an accomplishment, congratulations! Could
you explain to us in a nutshell: what is SUSE Enterprise Storage about?**

**Lars:** Our mission at SUSE is to support our customers in using Open Source
software to improve their business, and to improve Open Source software to
meet their needs; this guides our work on SUSE Enterprise Storage.

Software-defined Storage is a paradigm shift. Similar to how Linux displaced
commercial and proprietary Unix variants, it is now possible to build
infinitely scalable, flexible storage solutions with commodity hardware and
Open Source software. This offers more than just reduced CAPEX and OPEX. It
enables storage solutions that meet the customer's business needs better than
proprietary legacy products, and that can grow and adapt. All this while
avoiding vendor lock-in and integrating really nicely with the Cloud paradigm.
And, yes, it does so at a lower cost per capacity.

Ceph is a core component of this story; it's the most flexible, most scalable,
most vibrant Open Source project in this space. It provides the key storage
functionality - listing all it's features for block, object, and file storage
would exceed the scope of this interview. Suffice to say it is a truly
brilliant project, which is why SUSE has chosen it as the center piece.

**Lenz: So what are the benefits of using your distribution of Ceph instead
of just downloading and installing it? Could you tell us about some of the
improvements and contributions that SUSE has contributed to the Ceph
community?**

**Lars**: Well, Ceph does not exist in isolation. It runs on top of an
operating system (on both the client and the server). It is frequently used as
a storage back-end for a Cloud solution such as OpenStack, or consumed by
other applications and services, possibly on other operating systems.

Holistic testing from end-to-end is required. Management and monitoring
solutions that fit the customer's requirements (from DevOps to a management
dashboard). Documentation and training. And the assurance of reliable support
in case of issues.

SUSE, founded in 1992, has significant experience with providing this support
to our customers. We have answers for all the pieces in this stack - from the
SUSE Linux Enterprise Server operating system to SUSE OpenStack Cloud and SUSE
Manager, a world-class global support organization, we bring everything our
customer's need to succeed with SUSE Enterprise Storage. I may be biased, but
bringing Open Source technology to mission critical systems - we have done
this many times before and have an excellent track record.

Our work goes beyond the important and crucial packaging and integration
though. Our engineers are also contributing directly to the code in the
upstream community. Be it core components such as the Ceph monitors, release
management work, or the first to deliver feature-complete iSCSI gateways to
enable heterogeneous scenarios, our contributions are everywhere.

And as our storage teams continue to grow, our contributions will become more
and more visible. SUSE deeply understands the importance of community
collaboration for the success of Open Source projects. Thus, SUSE is also a
founding member of the Ceph Advisory Board, to advance the cooperation within
the Ceph community.

**Lenz:I agree that close collaboration with a project's developers and
community is a key to making a project successful and it is a key ingredient
of how we work at openATTIC as well. Speaking of our joint project - could you
briefly describe what brought you to openATTIC? How does it fit into your
vision?**

**Lars:** We were looking for an Open Source project that improved the
manageability of Ceph storage. We needed a framework that not only had the
right features, but also a solid, extensible code base and an active
development team.

openATTIC addresses many of the needs of our customers from Enterprise and
data center backgrounds. It already understands many tasks, and the RESTful
architecture allows use of its full power as a graphical web interface, via
the command line, or scripting it via python.

When we investigated the code, our engineers found that openATTIC 2.x met and
exceeded all our requirements; it's a modern foundation and we were sure that
we could enhance it to support managing and monitoring Ceph as well. Its clear
and simple Open Source license - the GNU General Public License - makes it
easy to contribute to.

And while integration and leveraging existing tools is key, we wanted
something that could be used without requiring a massive eco system or
all-encompassing management framework. It may sound paradoxical, but we were
also explicitly looking for a solution that was not exclusive to SUSE, but
that was portable to other Linux distributions.

Given that openATTIC has been under development for five years, shaped by the
actual business requirements of its users, we were confident that the existing
team was a strong partner to collaborate with. And, of course, we are adding
developers to that team and explicitly invite other members of the community
to join us!

**Lenz: I think I speak for the whole team if I say that we feel honored and
humbled by your decision to support and work with us. Can you elaborate on
some of the goals that we would like to achieve and how we plan to address
them?**

**Lars:** Our goal is simple - to make openATTIC the best and most popular
tool for managing and monitoring Ceph!

Of course, our first joint work was to get openATTIC into the `openSUSE Build
Service <https://build.opensuse.org/>`_ and the `Tumbleweed
<https://en.opensuse.org/Portal:Tumbleweed>`_ and `Leap
<https://en.opensuse.org/Portal:42.1>`_ distributions. As mentioned, our SUSE
Enterprise Storage project is fully built on Open Source, and this was the
first step towards inclusion there.

The next steps include extending the openATTIC models to understand Ceph more:
management of pools, RADOS Block Devices, adding Ceph's iSCSI gateway
functionality to openATTIC's iSCSI modeling, and object storage. To this end,
we work to utilize Ceph's calamari management to interface with Ceph itself;
we do not wish to reinvent the wheel here. Our joint work will likely result
in further upstream contributions to calamari itself.

Another cornerstone will be monitoring; a dashboard view, the ability to set
alarms on pools, and more. We also plan to - eventually - integrate openATTIC
with tools such as SaltStack to facilitate deployment and operational tasks.

**Lenz: So how will your engineers collaborate with the openATTIC team? What
benefits do you see for the openATTIC community?**

**Lars:** SUSE's engineering collaboration with the openATTIC team at it-novum
is completely out in the open. Our team chats on the open #openattic IRC
channel on Freenode and we use the public Google group and mailing list for
discussions. We track issues and planned features on the public JIRA instance.
All code is publicly accessible on BitBucket, and the current state of all
openSUSE packages on the openSUSE Build Service.

So really, if you have questions on where we are going, or feedback, or want
to contribute - it's really easy to `get involved
<http://openattic.org/get-involved.html>`_ and find out!

We hope that the openATTIC community benefits from this increase in the
developer base and contributions from SUSE, allowing openATTIC to become more
powerful, better, and available on more platforms.

And again, we want this to be very open and very inclusive. Our goals include
growing the community - both through presentations at community events (such
as our joint `presentation at the Linux Foundation's Vault conference
<http://sched.co/68k7>`_), as well as bringing more partners in and bringing
openATTIC to more users.

**Lenz: We look forward to this joint effort and hope that it will help us to
get more users and contributors excited about this project. Thanks a lot for
answering these questions and your support, Lars!**
