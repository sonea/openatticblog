.. title: openATTIC 2.0.16 beta has been released
.. slug: openattic-2016-beta
.. date: 2016-11-09 16:02:17 UTC+01:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the 2.0.16 beta release
.. type: text
.. author: Sebastian Krah

We're happy to announce the availability of openATTIC version 2.0.16!

Following our mantra "release early, release often", we have published a release four weeks after the release of 2.0.15. One of the highlights in this version is the migration support for openATTIC instances still running on Django 1.6!

Moreover, the openATTIC REST API does now report all installed packages as well as the currently installed openATTIC version. Most other changes are bug fixes and improvements of openATTIC. We have also continued working on supporting Ubuntu 16.04 LTS "Xenial Xerus".

.. TEASER_END

You can install openATTIC on Xenial by following these steps.

We highly recommend using the nightly packages, in order to ensure that fixes of currently known bugs are already included.

::

  # wget http://apt.openattic.org/A7D3EAFA.txt -q -O - | apt-key add -

  # cat > /etc/apt/sources.list.d/openattic.list << EOF
  deb http://apt.openattic.org/ xenial main
  deb-src http://apt.openattic.org/ xenial main
  deb http://apt.openattic.org/ nightly main
  deb-src http://apt.openattic.org/ nightly main
  EOF

  # apt-get update
  # apt-get install openattic
  # systemctl enable lvm2-lvmetad.socket
  # systemctl start lvm2-lvmetad.socket

`Known issues <https://tracker.openattic.org/issues/?jql=issuetype%20in%20(Bug%2C%20Sub-task)%20AND%20status%20in%20(Open%2C%20%22In%20Progress%22)%20AND%20resolution%20%3D%20Unresolved%20AND%20component%20in%20(Backend%2C%20Installation)%20AND%20labels%20%3D%20xenial%20ORDER%20BY%20priority%20DESC>`_:

* Its not possible to create any share on top of a volume (:issue:`OP-1697`)
* `targetcli` cannot be installed next to openATTIC (:issue:`OP-1694`)
* Its not possible to create a new host with iSCSI iqn or FC wwn (:issue:`OP-1696`)
* You have to run oaconfig install twice - otherwise oacli key is missing (:issue:`OP-1695`)
* Ubuntu Xenial: XML-RPCD: No JSON object could be decoded (:issue:`OP-1671`)
* `oaconfig install` requires lvm2 systemd services (:issue:`OP-1685`)
* Internal Server on `/api/pools` cause of ZFS error (:issue:`OP-1662`)

This release also provides some extensions to our Ceph integration on the backend side. For example,
we have continued working on our DeepSea integration.

Also, we can now migrate existing openATTIC databases on Ubuntu Xenial and SUSE which are still
based on Django 1.6. Which means you don't have to delete and recreate your database when you upgrade to newer Django versions.

To download and install openATTIC, please follow the `installation instructions
<http://docs.openattic.org/2.0/install_guides/index.html>`_ in the
documentation.

Please note that 2.0.16 is still a **beta version**. As usual, handle with care
and make sure to create backups of your data!

We would like to thank everyone who contributed to this release.

Your feedback, ideas and bug reports are very welcome. If you would like to get
in touch with us, consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group,
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave
comments below this blog post.

See the list below for a more detailed change log and further references. The OP
codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public Jira instance
<http://tracker.openattic.org/>`_.

Changelog for 2.0.16:

* Backend: The REST API 'host' resource returns the openATTIC version (in
  version.txt) if the host is an openATTIC host (:issue:`OP-553`)
* Backend: The REST API 'host' resource returns the installed Django apps if
  the host is an openATTIC host (:issue:`OP-1023`)
* Backend: Removed ID parameter from volume update view (:issue:`OP-1473`)
* Backend: Salt minions keys can now be accepted via our REST API (:issue:`OP-1601`)
* Backend: Reverted `NAGIOS_PLUGIN_DIR` on SUSE back to `/usr/lib/nagios/plugins`
* Backend: The backend does no longer print the whole traceback into the
  logfile if the XML file of an RRD file is not found (:issue:`OP-1632`)
* Backend: Django migration files aren't checked by Flake8 anymore (:issue:`OP-1638`)
* Backend: Added reading and writing of the `policy.cfg` of DeepSea REST API (:issue:`OP-1617`)
* Backend: The `is_oa_host` database field is set to `True` for oA hosts by 'oaconfig
  add-host' (:issue:`OP-1665`)
* Backend: Added database migrations for Django 1.6 (:issue:`OP-1649`)
* Backend: Fixed Bug on Ubuntu Trusty preventing deletion of iSCSI targets (:issue:`OP-722`)
* Backend: Fixed issue breaking the WebUI on Xenial (:issue:`OP-1689`)
* WebUI: Tabs will be selected as expected (:issue:`OP-1082`)
* Packaging: The `python-configobj` dependency is moved to the openattic-base
  package (:issue:`OP-1666`)
* Packaging: Added missing dependency `openattic-module-cron` in
  `openattic-module-lvm` (:issue:`OP-1664`)
