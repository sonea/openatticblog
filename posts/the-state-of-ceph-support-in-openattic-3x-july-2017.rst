.. title: Update on the state of Ceph Support in openATTIC 3.x (July 2017)
.. slug: the-state-of-ceph-support-in-openattic-3x-july-2017
.. date: 2017-07-07 08:05:06 UTC+02:00
.. tags: update, ceph, nfs, rgw, object storage, salt, deepsea 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

A little bit less than a month ago Lenz Grimmer :doc:`gave an overview <the-state-of-ceph-support-in-openattic-3x-june-2017>` 
about the current state of our development in openATTIC 3.x.

We made a lot of good progress in the meanwhile and I'm very proud to announce that NFS Gateway Management, RGW Bucket Management and Prometheus/Grafana made it into our newest 
`openATTIC 3.3.0 <https://build.opensuse.org/package/show/filesystems:openATTIC:3.x/openattic>`_ release as well as a lot of UI usability improvements. 

.. TEASER_END

The relationship between `DeepSea <https://github.com/SUSE/DeepSea>`_  and openATTIC is getting closer - that's why we recommend deploying and managing 
your Ceph Cluster with DeepSea to be able to use the full functionality and the latest features of openATTIC.

Currently we're working on an installation guide for DeepSea but for now you could take a look at the `README.md <https://github.com/SUSE/DeepSea/blob/master/README.md>`_ 
or my :doc:`blog post <create-ceph-cluster-with-deepsea>` about how to deploy DeepSea.

If you want to use a Grafana instance installed on a different node you just have to change the default settings in ``/etc/sysconfig/openattic``
to fit to your needs::

    # Host of the Grafana instance which shall be used by openATTIC.
    #GRAFANA_API_HOST="localhost"
    
    # Default port necessary as DeepSea doesn't provide one. Also, 80 is the default port of DeepSea.
    #GRAFANA_API_PORT="3000"
    
    # The username to log into Grafana.
    #GRAFANA_API_USERNAME="admin"
    
    # The password to log into Grafana.
    #GRAFANA_API_PASSWORD="admin"
    
    # The HTTP Scheme to be used. Either 'http' or 'https'.
    #GRAFANA_API_SCHEME="http"
    
For the next release we will be working on making the `Grafana settings configurable via the UI <https://tracker.openattic.org/browse/OP-2412>`_   

NFS Ganesha Management
------------------------------------

The management and configuration of NFS Gateway hosts (:issue: `OP-2195`) was released in oA 3.2.0. This feature
uses the `Salt REST API <https://github.com/saltstack/salt-api>`_ to communicate with DeepSea
for the required target configuration on the remote NFS nodes. This way openATTIC is now capable of managing NFS shares on top of
CephFS or S3 buckets on multiple nodes and supporting a lot of NFS features for NFSv3/4. The underlying NFS server functionality
is provided by `NFS Ganesha <http://nfs-ganesha.github.io/>`_.

.. slides::

        /galleries/oa-3.x-update-2017-07/oa-3.x-overview-nfs.png    
        /galleries/oa-3.x-update-2017-07/oa-3.x-overview-nfs2.png    
        /galleries/oa-3.x-update-2017-07/oa-3.x-nfs-cephfs.png    
    
Rados Gateway Bucket Management
------------------------------------------------

The management and configuration of RGW users and access keys (:issue: `OP-2368`) was released in oA 3.2.0. In 3.3.0,  we added
the functionality to manage RGW Buckets on multiple nodes.

.. slides::

        /galleries/oa-3.x-update-2017-07/oa-3.x-rgw-buckets2.png    
        /galleries/oa-3.x-update-2017-07/oa-3.x-rgw-buckets.png    

Ceph Monitoring with Grafana/Prometheus
-------------------------------------------------------

The decision was made (:issue: `OP-829`)! We replaced our current Nagios and pnp4nagios with `Grafana <https://github.com/grafana/grafana>`_ and 
`Prometheus <https://github.com/prometheus/prometheus>`_. You can switch between the different Dashboards or directly access OSD, Pool or Host
stats within the dedicated panel by selecting a specific item in the list. We're almost done regarding the UI-part, only the RBD graphs are currently still based on the old Nagios 
implementation. This will be removed as soon as we've written a Prometheus exporter for RBD's to be able to collect useful data. We're delivering four dashboards
by default which are:

* Ceph - Cluster (default) -  Gives you an overview about the current state of your cluster (Status Cluster, Status Monitors, Cluster Capacity...)
* Ceph - Pools - Details of a selected Pool (Objects, IOPS, Throughput)
* Ceph - OSD - Details of a selected OSD (Utilization, PGs, Utilization Variance, Latency, OSD Storage)
* Node Statistics - Details of a selected Host (CPU, Memory, Disk I/O, Filesytem Fullness...)

.. slides::

        /galleries/oa-3.x-update-2017-07/oa-3.x-dashboard-cluster.png    
        /galleries/oa-3.x-update-2017-07/oa-3.x-dashboard-cluster2.png    
        /galleries/oa-3.x-update-2017-07/oa-3.x-dashboard-osd.png    
        /galleries/oa-3.x-update-2017-07/oa-3.x-dashboard-pool.png    
        /galleries/oa-3.x-update-2017-07/oa-3.x-dashboard-nodes.png 

Next step is to remove Nagios from the `backend <https://tracker.openattic.org/browse/OP-2436>`_ as well.

UI Improvements
----------------------

We improved the UI in general and aim to improve the usability with every release. One highlight in this release is the "System -> Settings" Page where you
could specify your Salt API host as well as the credentials for the RGW hosts. We also added a live status/validation check of the configuration so you get direct feedback
if your settings are correct or not. 

.. slides::
    
        /galleries/oa-3.x-update-2017-07/oa-3.x-overview-settings.png    

Availability
--------------

Currently, openATTIC 3.x is available in packaged form for `openSUSE Leap
<https://www.opensuse.org/>`_ via the `filesystems:openATTIC:3.x/openattic
<https://build.opensuse.org/project/show/filesystems:openATTIC:3.x/openattic/>`_
package repository on the `openSUSE Build Service
<https://build.opensuse.org/>`_. As already mentioned in our last update, we plan to add
packages for other distributions as well but with our closer integration into DeepSea it depends on the 
availablility of DeepSea for these platforms. If anybody is willing to give us a hand on this, it would be 
fabulous and would help to speed up this process.

As usual, we appreciate any feedback or comments you might have - please :doc:`get in touch with us <get-involved>`!


