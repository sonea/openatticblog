.. title: Welcome to the openATTIC Team Blog!
.. slug: welcome to the openattic team blog
.. date: 2015-09-09 15:26:38
.. tags: announcement, news
.. category: 
.. link: 
.. description: Welcoming readers to the new openATTIC blog
.. type: text
.. author: Lenz Grimmer

Welcome to the openATTIC team blog!

On this blog, we will publish news and updates about `openATTIC
<http://www.openattic.org/>`_ as well as other articles related to Linux, and
Open Source Software in general, storage technologies, software development,
tools and so on.

This blog is based on the `Nikola <https://getnikola.com/>`_ static site
generator. In an upcoming post, we'll provide some more background on why we
chose this platform for our purposes.

That's all for now - thanks for stopping by!
