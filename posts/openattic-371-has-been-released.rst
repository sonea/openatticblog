.. title: openATTIC 3.7.1 has been released
.. slug: openattic-371-has-been-released
.. date: 2019-01-28 14:52:30 UTC+01:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource,
   release
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.7.1 release
.. type: text
.. author: Tatjana Dehler

We're happy to announce version 3.7.1 of openATTIC!

Version 3.7.1 is the first release in 2019, containing fixes for multiple issues
that were mainly reported by users.

Besides the bug fixes another big feature of this release is the updated Chinese
translation. Special thanks to Joël who submitted the pull request.

.. TEASER_END

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_
channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_
or leave comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our
`public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.7.1
---------------------------

Changed
~~~~~~~
* WebUI: Update chinese translations (:issue:`OP-3179`)
* WebUI: Disable statistic graph for RBD if fast-diff isn't enabled
  (:issue:`OP-3181`)

Fixed
~~~~~
* Backend: Fixed conversion of boolean string configuration values
  (:issue:`OP-3176`)
* WebUI: Fix displaying pool information (:issue:`OP-3177`)
* WebUI: Generation of API token for user doesn't work (:issue:`OP-3180`)
* Deployment: Adapt Vagrant config to setup a new box from scratch
  (:issue:`OP-3185`)
* Packaging: Remove executable bit from CHANGELOG file (:issue:`OP-3183`)
* Packaging: Correct some file permissions in openattic.spec file
  (:issue:`OP-3184`)
* Packaging: Revert `make_dist.py` to previous working version
  (:issue:`OP-3186`)