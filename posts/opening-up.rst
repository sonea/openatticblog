.. title: Opening up...
.. slug: opening-up
.. date: 2015-10-01 17:11:16 UTC+02:00
.. tags: news, opensource, collaboration
.. category: 
.. link: 
.. description: The openATTIC development 
.. type: text
.. author: Lenz Grimmer

In the past few days, we've made a number of significant changes to how
openATTIC is developed and managed. The main openATTIC source code repository
is now located on BitBucket, and we've introduced a new development/branching
process. We've also made the openATTIC Jira project open to the public, so
anyone can review existing isses and get a glimpse into what we're working on.

.. TEASER_END

We're trying to make our ongoing development work on openATTIC more
approachable and accessible to external users and developers. In order to
reach this goal, we've passed several mile stones in the past few days.

Before, the development on the openATTIC code base revolved around a Mercurial
repository that was hosted on an internal server. Eeach push triggered an
action that pushed every new ChangeSet to `our public BitBucket repository
<https://bitbucket.org/openattic/openattic>`_ as well, so the public repo was
always on the same level as the internal repo. However, this applied to the
default branch only - internal development or feature branches did not appear
on BitBucket.

In order to make better use of some BitBucket features like `Forks
<https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html>`_
and `Pull Requests
<https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html>`_,
we made the decision to transition our development focus to the Mercurial
repository on Bitbucket instead. The internal hg repo now only acts as a local
mirror for backup purposes, by pulling changes from BitBucket via a cron job.

While this is more a procedural than a technical change, it was an important
first step for us. Now, every ChangeSet is pushed into a repository hosted on
BitBucket directly.

Going forward, each developer should perform ongoing development in a personal
fork of the openATTIC main repository. This makes it much easier to work on
more complex features in separate branches and fosters collaboration, by
giving other developers direct access to the work in progress. We also plan on
making use of pull requests and the associated review functionality more
intensively, to improve the code quality and give other developers more
insight into other parts of the code base. New features and other more complex
changes will only be merged after a review has been performed.

We've also introduced a separate development branch for this, which will act
as a staging area for code that has been merged via a pull request or direct
push. New development work and feature branches will be derived from the
development branch. Only after all tests have passed, a merge from the
development branch into the default branch will be performed. Our goal is to
always keep the default branch in a tested, stable state (no tests failing),
so it could potentially be used to perform a release build at any time.

Last but not least we're working on opening up our currently internal Jira
issue tracking system for external developers and users. As a first step, the
`openATTIC project <https://project.it-novum.com/browse/OP>`_ is now publicly
visible, in read-only mode. Anyone can look at all the issues that we've
created, to get a better insight into what we're currently working on and
what's still in the pipeline. For now, bug reports still have to be `submitted
via BitBucket <https://bitbucket.org/openattic/openattic/issues>`_, but we've
applied for an Open Source license key for Jira, so hopefully we're no longer
limited by the user restriction of the current license soon.

If Atlassian approves our application, we'll migrate the openATTIC Jira
project to a dedicated instance on openattic.org and will enable issue
creation and commenting on existing issues. Pulling issues from BitBucket into
Jira is a tedious process (there is no direct synchronization), so this will
help us to reduce the number of "pockets of information" and to focus the
development efforts.

**Update (2015-10-09)** Atlassian approved our request for an Open Source
License in record time and we've now moved the openATTIC project to a new
shiny new dedicated Jira instance on http://tracker.openattic.org/. We're
very grateful for this generous offer from Atlassian. Thank you!
