.. title: openATTIC Wins a Silver OSBAR Award 2016
.. slug: openattic-wins-osbar-award-2016
.. date: 2016-12-07 16:34:20 UTC+01:00
.. tags: announcement, award, opensource, osbar
.. category:
.. link:
.. description: Announcing the grant of the OSBAR 2016
.. type: text
.. author: Lenz Grimmer

.. image:: /images/osbar-logo-small.png

Since 2014, `OSBAR <http://osbar.it>`_, the innovation award of the German `Open
Source Business Alliance <http://osb-alliance.de/>`_ (OSB Alliance), highlights
open source projects that add real benefit to the IT-world.

Submissions are assessed based on originality, innovation, practical relevance
and maturity by a committee of six well-known German IT- and open source
experts.

In total, 20 open source projects applied for this year's awards and one of them
was our open source Ceph and storage management framework.

.. TEASER_END

We're excited to announce that the openATTIC project yesterday was granted a
silver award!

.. thumbnail:: /galleries/OSBAR-2016/OSBAR-2.jpg
   :height: 240px
   :alt: Closeup of the OSBAR 2016 silver aware

.. thumbnail:: /galleries/OSBAR-2016/OSBAR-1.jpg
   :height: 240px
   :alt: Closeup of the OSBAR 2016 silver aware

This is a great honor for us and we would like to thank the OSBA and the Jury
for this recognition of our work.

The OSBAR winners were awarded during the `OPEN! 2016
<http://www.openkonferenz.de/>`_ event of OSB Alliance on December 7 in
Stuttgart, Germany. Volker Theile from SUSE's openATTIC engineering team
participated on our behalf and accepted the award.

.. thumbnail:: /galleries/OSBAR-2016/OSBAR-3.jpg
   :height: 240px
   :alt: Peter Ganten of the OSBA hands the award to Volker Theile

Congratulations to the openATTIC team and the other winners!

Thank you very much again for the award and special thanks to Volker Theile and
Felix Kronlage for contributing the pictures to this article.