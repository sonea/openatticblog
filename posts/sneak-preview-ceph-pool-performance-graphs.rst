.. title: Sneak Preview: Ceph Pool Performance Graphs
.. slug: sneak-preview-ceph-pool-performance-graphs
.. date: 2016-09-15 11:16:22 UTC+02:00
.. tags: ceph, preview, development, opensource
.. category:
.. link:
.. description: Preview of the upcoming Ceph pool monitoring
.. type: text
.. author: Lenz Grimmer

As I wrote in my :doc:`call for feedback and testing of the Ceph management
features <seeking-your-feedback-on-the-ceph-monitoring-and-management-functionality-in-openattic>`
in openATTIC 2.0.14, we still have a lot of tasks on our plate.

Currently, we're laying the groundwork for consuming SUSE's `collection of Salt
files <https://github.com/SUSE/DeepSea>`_ for deploying, managing and automating
Ceph. Dubbed the "DeepSea" project, this framework will form the foundation of
how we plan to extend the Ceph management capabilities of openATTIC to deploy
and orchestrate tasks on remote Ceph nodes.

In parallel, we are currently working on extending the openATTIC WebUI on making
the existing backend functionality accessible and usable. Next up is displaying
the performance statistics for Ceph pools that we already collect in the backend
(:issue:`OP-1405`).

To whet your appetite, here's a screen shot of the ongoing development:

.. thumbnail:: /images/oa-ceph-pool-statistics.png

Keep in mind this is work in progress. What do you think?