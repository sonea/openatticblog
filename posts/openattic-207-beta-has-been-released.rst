.. title: openATTIC 2.0.7 beta has been released
.. slug: openattic-207-beta-has-been-released
.. date: 2016-01-20 15:07:31 UTC+01:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release 
.. category: 
.. link: 
.. description: Announcing the openATTIC 2.0.5 beta release
.. type: text
.. author: Sebastian Krah

We are pleased to welcome you to the first openATTIC release for this year and are happy to announce openATTIC 2.0.7 beta! 
You may wonder: "What happened to version 2.0.6"? Due to a missing code merge, we decided to do 2.0.7 right away - sorry for the noise.

**Some highlights:**

For this release, we integrated the Django REST framework token authentication into our REST API, so you can request your authentication token via the API now and use it in scripts or other applications instead of the user name and password.

.. TEASER_END

For some time already, we are using `jshint <http://jshint.com/>`_ to keep our frontend code clean. In openATTIC 2.0.6/2.0.7 we have added `jscs <http://jscs.info/>`_ to match all of our coding conventions  (which will be publicly available soon). In a breath we have cleaned up the frontend code a lot.

We also updated and extended the documentation in order to make processes and how to's more helpful.

We would like to thank everyone who contributed to this release! Special thanks to Lars Marowsky-Brée and Joshua Schmid for giving us constructive feedback!

Please note that 2.0.7 is still a **beta version**. As usual, handle with care and make sure to create backups of your data!

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog 2.0.7

* WebUI: Fixed jshint and jscs errors (:issue:`OP-826`)
* WebUI/QA: extended host e2e test in order to check the correct title (see :issue:`OP-855`)
* Backend: Added possibility to see the own authentication token (if one is set) (:issue:`OP-833`)
* Backend: Added a detail route to create a new authentication token for an existing user (:issue:`OP-834`)
* Documentation: updated developer documentation by adding missing git install
* Documentation: added sections about authentication in general and how you can get the authentication token of your own openattic user (:issue:`OP-860`).

Changelog 2.0.6

* WebUI: Added grunt-jscs v2.5.0 (:issue:`OP-832`)
* WebUI: Updated grunt-contrib-jshint from 0.6.4 to 0.11.3 (:issue:`OP-825`)
* WebUI: Fixed jshint and jscs errors (:issue:`OP-826`)
* WebUI: fixed wrong title header in host edit action, see :issue:`OP-854`
* WebUI/QA: updated e2e wizard test by using the updated check_wizard titles helper function (:issue:`OP-828`)
* WebUI/QA: check fullscreen mode (see :issue:`OP-700` / :issue:`OP-811`)
* WebUI/QA: updated VM Storage wizard e2e (see :issue:`OP-843`)
* WebUI/QA: added wizard e2e test based on zfs/iscsi (see :issue:`OP-628`)
* Installation: Added cron to the requirements of the openattic-module-twraid RPM subpackage (:issue:`OP-845`)
* Backend: The authentication token of a user is now obtainable by correct username and password if the user is not logged in (:issue:`OP-841`)
* Code cleanup: Updated date in copyright headers (:issue:`OP-836`)
* Documentation: fixed package name and added protractor version (section E2E), see :issue:`OP-847`
