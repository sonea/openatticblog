.. title: openATTIC 2.0.8 beta has been released
.. slug: openattic-208-beta-has-been-released
.. date: 2016-03-01 15:16:31 UTC+01:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release 
.. category: 
.. link: 
.. description: Announcing the openATTIC 2.0.8 beta release
.. type: text
.. author: Patrick Nawracay

How time flies - it's time for our next release! Today, we released version 2.0.8 of openATTIC. Some highlights of this release:

In this version, we mainly improved the code quality, QA tests and usability of the openATTIC Web UI. For example, it's now possible to delete multiple volumes at once. We also removed some unnecessary options which could confuse the user.

In addition to that, we adjusted our tests according to the changes mentioned before and made further improvements to the JavaScript code base to be conformant with established coding conventions/standards (according to `jshint <http://jshint.com/>`_  and `jscs <http://jscs.info/>`_).

The build script ``make_dist.py`` is now included and can be used to create the "pristine source" tar archive (e.g. ``openattic-2.0.8.tar.bz2``), which acts as the input file for building RPM and DEB packages. The latter ones can now be built using the provided ``build_deb_packages.py`` script.

In the process of porting openATTIC to SUSE Linux (SLES 12 and Leap), we also started reworking and improving the configuration files, so openATTIC is more portable across different Linux distributions (in particular regarding the Nagios configuration). We'd like to thank Eric Jackson from SUSE for his contributions in this area!

.. TEASER_END

Known Issue: The Nagios package provided by the EPEL yum repository (used by RHEL and derivatives) was recently updated from version 3.x to version 4.0. Unfortunately, this update broke the installation and default configuration that openATTIC uses for setting up pnp4nagios: by default, openATTIC configures pnp4nagios to run in `Bulk Mode with npcdmod <http://docs.pnp4nagios.org/pnp-0.6/modes#bulk_mode_with_npcdmod>`_. As this mode is no longer supported by pnp4nagios with Nagios version 4, pnp4nagios needs to be configured to operate in `Bulk Mode with NPCD <http://docs.pnp4nagios.org/pnp-0.6/modes#bulk_mode_with_npcd>`_. See Jira ISSUE :issue:`OP-820` for more details and instructions on how to configure pnp4nagios on Red Hat Enterprise Linux (and derivatives like CentOS) to operate in this mode.

We also updated and extended the documentation in order to make certain processes and HOWTOs more helpful.

Please note that 2.0.8 is still a **beta version**. As usual, handle with care and make sure to create backups of your data!

We would like to thank everyone who contributed to this release! Special thanks to the folks from SUSE for their feedback and support.

 Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog 2.0.8

* WebUI: Allow deletion of multiple volumes (:issue:`OP-339`)
* WebUI: Display first and last name instead of username (:issue:`OP-792`)
* WebUI: Redirect a logged in user to from login to dashboard (:issue:`OP-799`)
* WebUI: Refresh snapshot panel instead of volume panel (:issue:`OP-817`)
* WebUI: Fixed all jshint and jscs errors (:issue:`OP-826`)
* WebUI: added copyright/license header (:issue:`OP-837`)
* WebUI: Don't allow the user to deactivate himself (:issue:`OP-851`) (thanks to Joshua Schmid for reporting)
* WebUI: Updated favicon to newer version (:issue:`OP-859`)
* WebUI: Adjust the volume protection dialog box to be intuitive (:issue:`OP-869`)
* WebUI: Visibility of volume tab menu (:issue:`OP-876`)
* WebUI: Show a delete button if multiple volumes are selected (:issue:`OP-884`)
* WebUI/QA: readded protractor zfs e2e test suits
* WebUI/QA: E2E test for :issue:`OP-851`, to verify that you can't deactivate your profile (:issue:`OP-852`)
* WebUI/QA: E2E test for deletion of multiple volumes (:issue:`OP-853`)
* WebUI/QA: E2E test for :issue:`OP-792`, to verify the displayed name (:issue:`OP-868`)
* WebUI/QA: Adjusted E2E tests with volume protection for :issue:`OP-869` (:issue:`OP-870`)
* WebUI/QA: Adapted E2E tests related to :issue:`OP-339` - Allow deletion of multiple volumes (:issue:`OP-871`)
* WebUI/QA: re-added commandlog check ('lvcreate') for specific volume (see :issue:`OP-873`)
* WebUI/QA: More tests for multiple selections (:issue:`OP-875`)
* WebUI/QA: fixed snapshot test related to :issue:`OP-817` (:issue:`OP-881`)
* WebUI/QA: extended all storage tab tests by checking the url :issue:`OP-891` - related to :issue:`OP-817` (:issue:`OP-891`)
* WebUI/QA: extended general e2e tests by checking the url when menu entry has been clicked (:issue:`OP-897`)
* WebUI/QA: Fix selection problem in multiple_volume_deletion (:issue:`OP-908`)
* WebUI/QA: Adapt :issue:`OP-884`-related e2e tests (:issue:`OP-916`)
* Backend: added copyright/license header to gatling files (:issue:`OP-844`)
* Backend/Gatling: Removed colorizer.py from the Gatling API test framework due to licensing concerns (:issue:`OP-924`)
* Documentation: updated documentation (section e2e, :issue:`OP-910`)
* Installation: Adding distro specific settings for monitoring (:issue:`OP-842`) (thanks to Eric Jackson for the contribution)
* Installation: Fixed installation errors on EL7 that were caused be the update of Nagios to version 4 in the EPEL package repository. Currently, pnp4nagios  needs to be configured manually for the performance graphs to be displayed.  (:issue:`OP-820`)
* Installation: Updated and improved the installation and content of the default configuration file /etc/default/openattic (:issue:`OP-920`)
* Installation: Trimmed down the list of required Nagios plugins in the RPM package (:issue:`OP-942`)
* Packaging: Included the build scripts into the official repository. They can be used to create a tar archive and with that, the corresponding Debian or RPM packages. (:issue:`OP-892`)
* Packaging: Introduced a new command line switch for build_deb_packages.py to update a local repository with the newly built packages (:issue:`OP-948`)
