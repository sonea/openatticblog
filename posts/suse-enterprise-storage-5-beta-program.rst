.. title: SUSE Enterprise Storage 5 Beta Program
.. slug: suse-enterprise-storage-5-beta-program
.. date: 2017-06-11 14:15:49 UTC+02:00
.. tags: beta, ceph, community, suse, testing
.. category: 
.. link: 
.. description: Announcing the SUSE Enterprise Storage 5 beta program
.. type: text
.. author: Lenz Grimmer

:doc:`openATTIC 3.x <the-state-of-ceph-support-in-openattic-3x-june-2017>` will
be part of the upcoming SUSE Enterprise Storage 5 release, which is currently in
beta testing. It will be based on the upstream Ceph "Luminous" release and will
also ship with openATTIC 3.x and Salt/DeepSea for the orchestration, deployment
and management.

If you would like to take a look at this release and help us with testing the
new functionality provided by openATTIC and DeepSea without having to assemble
the various pieces manually, please join our beta test program by following the
instructions outlined on the `SUSE Enterprise Storage 5 Beta Program
<https://www.suse.com/betaprogram/storage-beta/>`_ web page.

We look forward to your feedback!