.. title: Deprecating support for Ubuntu 14.04 LTS Trusty
.. slug: deprecating-support-for-ubuntu-1404-lts-trusty
.. date: 2017-03-02 16:38:12 UTC+01:00
.. tags: support ubuntu trusty
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

Now with Ubuntu 16.04 LTS "Xenial" being available since quite some
time, we intend to slowly drop Ubuntu 14.04 "Trusty" as a supported
platform.

This will allow us to reduce the amount of testing required on our end
and also makes it possible to remove support for some "legacy"
technologies like SysV init (all other supported distributions are based
on systemd now), which will help to simplify the code base.

If you still use openATTIC on Ubuntu Trusty, please consider upgrading
your operating system to Ubuntu's latest LTS support in the near future.

If you have any troubles with upgrading openATTIC just `contact <http://openattic.org/get-involved.html>`_ us.
