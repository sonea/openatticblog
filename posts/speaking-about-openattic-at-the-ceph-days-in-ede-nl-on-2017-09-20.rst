.. title: Speaking about openATTIC 3.x at the Ceph Days in Ede (NL) on 2017-09-20
.. slug: speaking-about-openattic-at-the-ceph-days-in-ede-nl-on-2017-09-20
.. date: 2017-09-13 09:45:00 UTC+02:00
.. tags:  ceph, community, conference, development, openattic, storage
.. category: 
.. link: 
.. description: Speaking about openATTIC 3.x at the Ceph Day in Ede, NL on 2017-09-20
.. type: text
.. author: Lenz Grimmer

.. image:: http://ceph.com/wp-content/uploads/2016/07/logo_cephdays.png

After having been heads down with the development of openATTIC 3.x in the past
few months, it's time to get back to the surface and start talking about our
latest achievements!

In addition to speaking about openATTIC 3.x at `SUSECON in Prague
<http://www.susecon.com/>`_ later this month, I'll be traveling to the
Netherlands next week, to attend the `Ceph Day Netherlands
<http://ceph.com/cephdays/netherlands2017/>`_.

Quoting the `Ceph Days <http://ceph.com/cephdays/>`_ web site:

    Hosted by the Ceph community (and our friends) in select cities around the
    world, Ceph Days are full-day events dedicated to fostering our vibrant
    community. In addition to Ceph experts, community members, and vendors,
    you’ll hear from production users of Ceph who’ll share what they’ve learned
    from their deployments.

I last spoke about the Ceph features in openATTIC 2.x at the :doc:`Ceph Day in
Munich <speaking-about-openattic-at-the-ceph-days-in-munich-2016-09-23>` almost
exactly a year ago, and a lot has happened with our project in the meanwhile. I
look forward to giving an update!

The title of my talk will be "Ceph Management and Monitoring with openATTIC
3.x", covering the following topics:

    With version 3.x, the openATTIC project has significantly changed its
    direction of development and underwent a few major changes. In this session,
    Lenz will give an overview about the current state of development and the
    Ceph management functionality provided in version 3 of openATTIC. He will
    highlight the differences to version 2.x and give an overview about how the
    new components are being utilized in order to help managing and monitoring
    Ceph clusters.

The registration for Ceph Day NL is still open, so make sure to `register now!
<https://www.eventbrite.com/e/ceph-day-netherlands-tickets-31538597795>`_