.. title: openATTIC 2.0.13 beta has been released
.. slug: openattic-2.0.13-beta-has-been-released
.. date: 2016-07-29 12:51:13 UTC+02:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 2.0.13 beta release
.. type: text
.. author: Sebastian Wagner

openATTIC 2.0.13 beta has been released

We're happy to announce the availability of openATTIC version 2.0.13!

In this release, we have made further improvements regarding the Ceph RBD handling in our user interface. We cleaned up many Ceph related detail-information tabs in order to display only useful data, especially on the Ceph RBD Page. 
We've also made some usability improvements on our dashboard - unfinished wizard steps have been removed and share paths will be
set automatically at the end of the wizard instead of bothering the user with that.
We also added a new dialog for creating new RBDs and integrated the functionality to delete RBDs within the UI. See our `Sneak preview of additional Ceph RBD management functions <https://blog.openattic.org/posts/sneak-preview-of-additional-ceph-rbd-management-functions/>`_ for details.

The Nagios monitoring has been improved by adding performance data for Ceph pools. Also, we are constantly tracking 
the responsiveness of a Ceph cluster now.

In our Ceph-related backend part, we made some performance improvements, by running only the commands which are 
actually used by the REST API.

You may want to make/store the REST-API accessible on another host. In 2.0.13 it's possible to configure the url of the API globally. So, you don't need to customize every service which calls the api.

For those of you, who want to use openATTIC on a preconfigured VM, we're now providing VMs for KVM and VirtualBox, which can be found at `apt.openattic.org/vms/ <http://apt.openattic.org/vms/>`_. 


If you already run the ``check_cephcluster`` Nagios plugin, you might receive the following error in your PNP4Nagios log (``/var/log/pnp4nagios/perfdata.log``):

   2016-07-22 14:15:14 [22939] [0] RRDs::update ERROR <path to the RRD file>: found extra data on update argument: 13.67
    
This is because of the new parameter ``exec_time``. In that case you will have to remove all existing RRD and XML files of your Ceph clusters.
This can be done by running:

   rm /var/lib/pnp4nagios/perfdata/<host_name>/Check_CephCluster_*
   
Note, After removing these files all collected performance data for your Ceph clusters so far are gone!

.. TEASER_END

To download and install openATTIC, please follow the `installation instructions <http://docs.openattic.org/2.0/install_guides/index.html>`_ in the documentation.

Please note that 2.0.13 is still a **beta version**. As usual, handle with care and make sure to create backups of your data!

We would like to thank everyone who contributed to this release! Special thank the folks from SUSE for their continued feedback and support.

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog 2.0.13:

* Backend: Added a REST API (``userprofiles``) to store user preferences (key/value pairs) (:issue:`OP-756`)
* Backend: Added the performance data (in JSON) of a Ceph cluster to the REST API (:issue:`OP-1279`)
* Backend: General Ceph RBD improvements (:issue:`OP-1309`, :issue:`OP-1302`, :issue:`OP-1305` and :issue:`OP-1133`)
* Backend: Improved compatibility with Ceph Hammer (:issue:`OP-1303`)
* Backend: Fixed RBDs with same name in multiple pools (:issue:`OP-1313`)
* Backend: Fixed ``ceph df`` missing a newly created pool (:issue:`OP-1282`)
* Backend: Optimized calls to librados (:issue:`OP-1321`)
* Backend: Added Ceph pool Nagios monitoring (:issue:`OP-1292`)
* Backend: The ``check_cephcluster`` Nagios plugin contains another value ``exec_time``. It represents the run time of the cluster check. (:issue:`OP-1307`)
* WebUI: Wizards automatically setting the share path (:issue:`OP-590`)
* WebUI: Make the openATTIC API URL configurable (:issue:`OP-989`)
* WebUI: Added deletion of a RBD or multiple RBDs (:issue:`OP-1032`)
* WebUI: Added creation of RBDs (:issue:`OP-753` & :issue:`OP-1290`)
* WebUI: MAde RBD table sortable and a cleaned up details view (:issue:`OP-1309`)
* WebUI: Added a welcome message to the login screen (:issue:`OP-1310`)
* WebUI: Enable cluster selection in the RBD creation form (:issue:`OP-1314`)
* WebUI: Added grunt task "grunt inspect --force" to check the quality of your JavaScript code (:issue:`OP-1332`)
* WebUI: Replacing the "`Sorry folks`" messages with loading messages (:issue:`OP-1333`)
* WebUI: Add form throws toasty error message in case of a pool list loading failure. (:issue:`OP-1357`)
* WebUI/QA: RBD form, table, details, creation and deletion tests (:issue:`OP-1338`)
* WebUI/QA: Restructured wizard tests with help of a page object (:issue:`OP-1356`)
* WebUI/QA: Includes cluster selection tests in the RBD creation form tests (:issue:`OP-1367`)
* Packaging: Removed dependency on ``python-simplejson`` from RPM and DEB packages (:issue:`OP-1106`)
* Packaging: Remove ``backend/.style.yapf`` from the RPM build root instead of packaging it (:issue:`OP-1141`)
* Packaging: Added ``version.txt`` to the ``openattic-base`` RPM and DEB files (:issue:`OP-1267`)
* Packaging: Fixed building tar archives from a local mercurial repository (:issue:`OP-1312`)
* Packaging: Enabled relative paths for ``make_dist.py`` (:issue:`OP-1324`)
* Packaging: Fixed automatic Debian changelog adaption (:issue:`OP-1288`)
* Packaging: Fix building Debian snapshot packages on Ubuntu 16.04 (:issue:`OP-1372`)
