.. title: openATTIC 3.6.1 has been released
.. slug: openattic-361-has-been-released
.. date: 2017-12-14 18:20:23 UTC+01:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release 
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.6.1 release
.. type: text
.. author: Lenz Grimmer

It is our great pleasure to announce Version 3.6.1 of openATTIC.

3.6.1 is a bugfix release for the 3.6 stable branch, containing fixes for multiple issues
that were reported by users.

In addition to that, it contains several usability enhancements and security improvements.

Behind the scenes, we continued with converting the WebUI code into Angular components in
preparation for moving to Angular 2 in the near future.

Note that this release depends on a `DeepSea <https://github.com/SUSE/DeepSea>`_
version 0.8.2 or higher, as one improvement to the iSCSI target management
(:issue:`OP-2926`) required making changes to both openATTIC and DeepSea.

.. TEASER_END

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_
channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_
or leave comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our
`public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.6.1
---------------------------

Added
~~~~~
* Backend/QA: Increased unit test coverage of rest and userprefs app (:issue:`OP-2809`)
* WebUI: Add a 'Clear' button to the datatable search field (:issue:`OP-2900`)
* WebUI: Add a timestamp of last update on the dashboard (:issue:`OP-2837`)
* Development: Import ``set_symlinks.sh`` in order to create symlinks for
  building a development environment (:issue:`OP-2899`)
* Backend/WebUI: Manage iSCSI service per node (:issue:`OP-2763`)
* Backend/WebUI: Check for DeepSea version requirements (:issue:`OP-2543`)

Changed
~~~~~~~
* Backend/QA: Improve auth Gatling tests to check the browser cookie
  expiry (:issue:`OP-2921`)
* WebUI: Migration of Dashboard module into Angular (:issue:`OP-2861`)
* WebUI: Migration of Ceph Erasure Code Profiles module into Angular (:issue:`OP-2854`)
* WebUI: Migration of Navigation module into Angular (:issue:`OP-2864`)
* WebUI: Migration of Auth module into Angular (:issue:`OP-2851`)
* WebUI: Migration of Notification module into Angular (:issue:`OP-2865`)
* WebUI: Migration of Settings module into Angular (:issue:`OP-2867`)
* WebUI: Migration of Ceph Nodes module into Angular (:issue:`OP-2857`)
* WebUI: Migration of Ceph Crushmap module into Angular (:issue:`OP-2853`)
* WebUI: Migration of Shared module into Angular (:issue:`OP-2868`)
* WebUI: Migration of User Info module into Angular (:issue:`OP-2870`)
* WebUI: Migration of Ceph NFS module into Angular (:issue:`OP-2856`)
* WebUI: Migration of Ceph OSDs module into Angular (:issue:`OP-2858`)
* WebUI: Migration of Ceph ISCSI module into Angular(:issue:`OP-2855`)
* WebUI: Migration of Ceph Pools module into Angular (:issue:`OP-2859`)
* WebUI: Migration of OA module into Angular (:issue:`OP-2904`)
* WebUI: Migration of Task Queue module into Angular (:issue:`OP-2869`)
* WebUI: Improve Object Gateway user/bucket filtering (:issue:`OP-2912`)
* WebUI: Refactor ceph-nfs-state service to improve execution time (:issue:`OP-2882`)
* WebUI: The datatable does not show any progress if filter options are
  changed (:issue:`OP-2920`)
* WebUI: Display meaningful error when creating a RGW user using an existing
  email address (:issue:`OP-2902`)
* Backend/WebUI: Usage of task queue on RBD deletion to prevent timeout errors
  when deleting large RBD images. (:issue:`OP-2742`)
* WebUI: Migration of Users module into Angular (:issue:`OP-2871`)
* WebUI: Migration of Ceph RBD module into Angular (:issue:`OP-2860`)
* WebUI: Object Gateway Buckets now provides endpoint addresses (:issue:`OP-2908`)
* WebUI: Replace _.isObject with _.isObjectLike (:issue:`OP-2955`)
* Backend/WebUI: Usage of task queue on iSCSI service management to prevent
  timeouts (:issue:`OP-2788`)
* Development: Make ``make_dist.py`` always use the current HEAD revision
  (:issue:`OP-2883`)

Fixed
~~~~~
* WebUI: Get all frontend unit tests working again (:issue:`OP-2889`)
* Backend: Fix namespace issue with backend/views.py (:issue:`OP-2896`)
* Backend: Node search field isn't working (:issue:`OP-2886`)
* Backend: Performance Improvements when listing RBDs (:issue:`OP-2688`)
* Backend: Fixed Internal Server Error when listing RBDs with missing
  (:issue:`OP-2895`)
* Backend: Fixed an error message in the openATTIC log file when
  issuing `oaconfig install` (:issue:`OP-2898`)
* Backend: Made Gatling tests more idempotent (:issue:`OP-2913`)
* Backend/WebUI: Fixed the limitation of the RBD dropdown list to 50
  elements in the iSCSI add form (:issue:`OP-2907`)
* WebUI: Fix current user validation on page load (:issue:`OP-2887`)
* WebUI: Fix random javascript error on RBD form (:issue:`OP-2824`)
* WebUI: Display error message when no cluster exists (:issue:`OP-2948`)
* Installation: Removed executable bit from CHANGELOG (:issue:`OP-2934`)
* Installation: oaconfig: Only parse required settings from config file
  (:issue:`OP-2985`)
* WebUI: Fix problem with persistent notification options (:issue:`OP-2952`)
* WebUI: Fix typo in settings notification (:issue:`OP-2953`)
* Backend: Fix "test_bucket_get" Django unit test (:issue:`OP-2947`)
* WebUI: Fix javascript error after remove RBD size (:issue:`OP-2966`)
* WebUI: Hide striping preview until RBD size is set (:issue:`OP-2968`)
* WebUI: Fix Notification call (:issue:`OP-2978`)
* Backend/WebUI: Adding an iSCSI target in one iSCSI gateway causes the lrbd
  service to restart all iSCSI targets (:issue:`OP-2926`)
* WebUI: Fix typo on Ceph OSD scrub buttons (:issue:`OP-2988`)

Removed
~~~~~~~
* WebUI: Removed unnecessary heading in RGW name validation unit
  tests (:issue:`OP-2901`)
* Packaging: Removed creation of /var/lock/openattic (and /run/lock/openattic
  respectively) during installation (:issue:`OP-2946`)

Security
~~~~~~~~
* Backend: Migrated .secret.txt into a setting in the settings file (:issue:`OP-2873`)
* Installation: Secure permissions on sysconfig file (:issue:`OP-2949`)
