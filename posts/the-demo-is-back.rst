.. title: The demo is back!
.. slug: the-demo-is-back
.. date: 2017-12-08 09:24:52 UTC+01:00
.. tags: demo ceph 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

We're happy to announce that our demo is up and running again.

The demo will be reinstalled every day at midnight.
The login credentials are **openattic/openattic**. With the next release
we'll include the demo mode which allows us to show this information at the
top of the demo.

So please take the chance to visit `demo.openattic.org <http://demo.openattic.org>`_
