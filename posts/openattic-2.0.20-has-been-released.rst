.. title: openATTIC 2.0.20 has been released
.. slug: openattic-2020-has-been-released
.. date: 2017-05-03 10:00:00 UTC+01:00
.. tags: announcement, ceph, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the openATTIC 2.0.20 release
.. type: text
.. author: Lenz Grimmer

It is our great pleasure to announce the release of openATTIC version 2.0.20.
This is a minor bugfix release, which also provides a number of small selected
improvements, e.g. in the WebUI (styling, usability), installation and logging
(now adds PID and process name to logs). Furthermore, we updated our
documentation - especially the installation instructions as well as the
developer documentation.

.. TEASER_END

Your feedback, ideas and bug reports are very welcome. If you would like to get
in touch with us, consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group,
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave
comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public Jira instance
<http://tracker.openattic.org/>`_.

Changelog for version 2.0.20
----------------------------

Added
~~~~~
* Development: Added Ceph support to the Vagrant configuration
  (:issue:`OP-2048`)
* Development: Support for 'variable' id attribute on 'required' directive
  (:issue:`OP-1993`)
* Development: Added delete confirmation modal component (:issue:`OP-2013`)
* WebUI: Improved javascript error handling by notifying the user via toasty
  error messages (:issue:`OP-1926`)
* Documentation: Split installation instructions for Debian and Ubuntu, to
  make use of Ubuntu's ``add-apt-repository``
* Documentation: Instructions to backport commits into a stable release branch
  (:issue:`OP-2063`)

Changed
~~~~~~~
* WebUI: Upgrade angular-bootstrap to the newer major version 2.5.0
  (:issue:`OP-1888`)
* WebUI: Improved style of iSCSI iqn and fibre channel wwn inputs
  (:issue:`OP-1959`)
* Backend: Stop spamming the log with ``Skipping
  /etc/ceph/ceph.client.admin.keyring, permission denied`` (:issue:`OP-2066`)
* Backend: Improved logging by adding PID, Process Name and Current Version
  (:issue:`OP-2065`)

Fixed
~~~~~
* Installation: fixed "OAUSER: unbound variable" error during update on EL7
  (:issue:`OP-2046`)
* Installation: Fixed config file location in the SUSE systemd service
  (:issue:`OP-2067`)
* Backend: Fixed "Systemd did not respond to DBus ping" error on SUSE
  (BNC#1032263)
* Backend: Removed ``__getattribute__`` of ``NodbQuerySet``, cause of strange
  exceptions (:issue:`OP-2068`)
* WebUI: Prevents browser freezing when session expires (:issue:`OP-1779`)
* WebUI: Disallow underscores in IQN WWNs (:issue:`OP-1669`)
* WebUI: Fix incomplete delete confirmation message (:issue:`OP-2061`)
* Documentation: Updated developer documentation for git (:issue:`OP-2054`)
* Packaging: ``make_dist.py``: Fixed building of non-local git branches
  (:issue:`OP-2106`)
* Development: Fixed Django unit tests (:issue:`OP-1983`)
