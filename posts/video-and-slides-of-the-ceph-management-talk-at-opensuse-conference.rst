.. title: Video and Slides of the Ceph Management talk at openSUSE Conference
.. slug: video-and-slides-of-the-ceph-management-talk-at-opensuse-conference
.. date: 2016-06-27 07:36:21 UTC+02:00
.. tags: ceph, community, conference, opensuse, opensource, presentation, development, event
.. category:
.. link:
.. description: Announcing the availability of the slide deck and video recording for #osc16
.. type: text
.. author: Lenz Grimmer

Last week, I attended the `openSUSE Conference 2016
<https://events.opensuse.org/conference/oSC16/>`_ in Nuremberg, to give a
presentation about Ceph and Storage Management with openATTIC. Thanks a lot
for the conference organizers for inviting me, I had a good time there!

The `sessions <https://events.opensuse.org/conference/oSC16/schedule>`_ were
also streamed live and they uploaded the recordings to `C3TV
<https://media.ccc.de/b/conferences/osc16>`_ and `YouTube
<https://www.youtube.com/playlist?list=PL_AMhvchzBaeIQntCDiVNUUgmRaAzam1V>`_
in record time, much appreciated!

The video of `my presentation
<https://events.opensuse.org/conference/oSC16/program/proposal/924#1>`_ can be
found `here (C3TV)
<https://media.ccc.de/v/924-ceph-and-storage-management-with-openattic#video&t=805>`_
and `here (YouTube)
<https://www.youtube.com/watch?v=Q8FCp17Ch1g&list=PL_AMhvchzBaeIQntCDiVNUUgmRaAzam1V&index=58>`_:

.. youtube:: Q8FCp17Ch1g

I also uploaded my slide deck to SlideShare:

.. media:: http://www.slideshare.net/LenzGr/ceph-and-storage-management-with-openattic-opensusecon

Enjoy!
