.. title: openATTIC 3.5.1 has been released
.. slug: openattic-351-has-been-released
.. date: 2017-10-06 12:15:06 UTC+02:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.5.1 release
.. type: text
.. author: Tatjana Dehler

We are happy to announce Version 3.5.1 of openATTIC.

As you can see from the length of the fixed bug section in the changelog, we've been focused on
refining existing functionality and features.

**More significant changes include the addition of:**

* Graphical RBD striping preview
* Display OSDs list for each Ceph node
* Scrub option to Ceph nodes and OSDs
* Introduction of frontend unit tests
* Documentation chapter about unit tests in Python and how to configure and run them

**Some notable bug fixes include:**

* Update of our proxy implementation to support Grafana 4.5.1
* Fixed the openATTIC settings page to be accessible even without a valid keyring file
* Fixed pool size value in the pool overview table

In addition, we've replaced the web UI build tool 'Grunt' and package manager 'Bower' with a more
modern solution called `Webpack <https://webpack.js.org/>`_, which will also make the migration
to the latest Angular version easier in the future.

.. TEASER_END

We would like to thank everyone who contributed to this release.
Your feedback, ideas and bug reports are very welcome.
If you would like to get in touch with us, consider either joining our `openATTIC Users`_ Google Group,
visiting our `#openattic`_ channel on `irc.freenode.net`_ or leaving comments below this blog post.
See the list below for a more detailed changelog and further references.
The OP codes in brackets refer to individual Jira issues that provide additional details on each item.
You can review these on our `public JIRA instance`_.

Changelog for version 3.5.1
---------------------------

Added
~~~~~
* Backend: Added routes to trigger scrub per OSD, single host and many hosts
  (:issue:`OP-2603`)
* WebUI/Backend: Log frontend exceptions into openattic.log (:issue:`OP-2695`)
* WebUI: Add scrub option to Ceph Nodes and OSDs (:issue:`OP-2613`)
* WebUI: Added RBD striping graphical preview (:issue:`OP-2654`)
* WebUI: Display OSDs list for each Ceph Node (:issue:`OP-2273`)
* WebUI/QA: Add missing validation test for the RBD form (:issue:`OP-2693`)
* WebUI/QA: Add missing cleanup to e2e suites (:issue:`OP-2635`)
* WebUI/QA: Add plugin to stop protractor after first failed test (:issue:`OP-2694`)
* WebUI: Introduced frontend unit test (:issue:`OP-952`)
* Documentation: Add Python Unit Tests chapter to the Developer Documentation
  (:issue:`OP-2739`)

Changed
~~~~~~~
* WebUI: Improve taskQueueSubscriber (:issue:`OP-2674`)
* WebUI: Replace grunt for webpack (:issue:`OP-2566`)
* WebUI: Replace bower for npm (:issue:`OP-2567`)
* WebUI: Hiding the cluster selection in RBD form if only one cluster exists
  (:issue:`OP-2684`)
* WebUI: Update AngularJS to 1.5.11 (:issue:`OP-2709`)
* WebUI/QA: Move e2e folder into webui (:issue:`OP-2645`)
* WebUI: Convert NFS module to ES6 (:issue:`OP-2532`)
* WebUI: Replace interval with event listener for grafana resize (:issue:`OP-2724`)

Fixed
~~~~~
* WebUI/QA: Fix task queue waiting errors (:issue:`OP-2629`)
* Backend: Fixed `osd pool set`: unrecognized variable `crush_ruleset`
  (:issue:`OP-2651`)
* Backend: Nodes Tab may not list IPs for hosts without OSDs (:issue:`OP-2679`)
* Backend: The settings page will be accessible even if no valid keyring file
  was found automatically (:issue:`OP-2687`)
* Backend: Fix unable to disable pause flag (:issue:`OP-2702`)
* Backend: Fixed the Grafana proxy implementation for the updated
  Grafana version 4.5.1 (:issue:`OP-2705`)
* Backend: Added blank .py files in order to prevent anyone from importing old
  \*.pyc files (:issue:`OP-2700`, :issue:`OP-2699`)
* Backend: Fixed warning in `oaconfig`, if a keyring or keyring user has been
  set in `/etc/sysconfig/openattic` (:issue:`OP-2732`)
* Backend: Hidden dashboard select box of Grafana on various pages (:issue:`OP-2746`)
* Backend: 'keyring_or_none' will now check if an available keyring file is
  readable for the openATTIC user (:issue:`OP-2757`)
* WebUI/QA: Fix Cluster-wide OSD Flags e2e failures (:issue:`OP-2668`)
* WebUI/QA: Harden task queue deletion test cases (:issue:`OP-2626`)
* WebUI: Fix ugly borders in the task queue table (:issue:`OP-2491`)
* Backend: Fixed OSD table filtering (:issue:`OP-2692`)
* WebUI/QA: Fix random NFS e2e add export failure (:issue:`OP-2672`)
* WebUI/QA: Fixed duplicated CSS test class in RBD form (:issue:`OP-2721`)
* WebUI: Add context root to the webpack configuration (:issue:`OP-2697`)
* WebUI: Fix missing html files (:issue:`OP-2698`)
* WebUI/QA: Increased test task times in the directive suite (:issue:`OP-2671`)
* WebUI: Showing the right pool size in the pool table (:issue:`OP-2710`)
* Development: Stop dev server refreshes on vim changes (:issue:`OP-2715`)
* WebUI: Not all graphs are displayed on the RBD statistics page (:issue:`OP-2716`)
* WebUI: Fixes pool size table tooltip (:issue:`OP-2539`)
* WebUI: Improve error handling of MON-widget (:issue:`OP-2658`)
* WebUI: Removed error message when accessing iSCSI without Salt-API (:issue:`OP-2728`)
* WebUI: Add button to show/hide passwords on the settings page (:issue:`OP-2734`)
* Packaging: Fixed make_dist.py error with particular switches used (:issue:`OP-2628`)
* Packaging: Updated `make_dist.py` to reflect the changes since the
  introduction of webpack and the moved E2E folder (:issue:`OP-2717`)

Removed
~~~~~~~
* WebUI: Refactored node service (:issue:`OP-2122`)
* WebUI: Remove the deprecated oaTabView module (:issue:`OP-2706`)
* Development: Removed old `virtualenv` requirements.txt entries (:issue:`OP-2675`)
* Development: Remove Support for Django 1.7 (:issue:`OP-2708`)
* Backend: Removed unnecessary %used calculation workaround for
  Luminous (:issue:`OP-2370`)
* Backend: Removed Hosts model (:issue:`OP-2589`)
* Backend: Cleanup systemd module (:issue:`OP-2677`)
* Backend: Remove `cmdlog` Module (:issue:`OP-2683`)
* Documentation: Remove Django 1.6 database migration documentation (:issue:`OP-2723`)

.. _openATTIC Users: https://groups.google.com/forum/#%21forum/openattic-users
.. _#openattic: irc://irc.freenode.org/#openattic
.. _irc.freenode.net: http://webchat.freenode.net/?channels=openattic
.. _public JIRA instance: http://tracker.openattic.org/