.. title: openATTIC 3.7.0 has been released
.. slug: openattic-370-has-been-released
.. date: 2018-08-08 11:23:37 UTC+02:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release
.. category:
.. link:
.. description: Announcing the openATTIC 3.7.0 release
.. type: text
.. author: Sebastian Krah 

We're happy to announce version 3.7.0 of openATTIC!

Version 3.7.0 is the first bugfix release of the 3.7 stable branch, containing fixes for multiple issues that were mainly reported by users.

There has been an issue with self-signed certificates in combination with the RGW proxy which is now configurable. We also improved the openATTIC user experience and adapted some of our frontend tests in order to make them more stable.

As mentioned in our `last blog post <https://www.openattic.org/posts/openattic-362-has-been-released/>`_ our team was working on a Spanish translation. We are very proud to have the translation included in this release. Thank you Gustavo for your contribution.

Another highlight of the release is then newly added RBD snapshot management. openATTIC is now capable to create, clone, rollback, protect/unprotect and delete RBD snapshots. In addition it is also possible to copy RBD images now.
Furthermore the "pool edit" feature received a slight update: we implemented the option to set the "EC overwrite" flag when editing erasure coded pools.

.. TEASER_END

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_
channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_
or leave comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our
`public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.7.0
---------------------------
    
Added
~~~~~
* WebUI/backend: added RBD snapshot management (create, clone, rollback,
  protect/unprotect, delete) (:issue:`OP-2569`)
* WebUI/backend: added support for copying RBD images (:issue:`OP-3150`)
* WebUI: Add spanish translation to openATTIC (:issue:`OP-3165`)

Changed
~~~~~~~
* WebUI/QA: Reduced PG creation during e2e pool creation tests (:issue:`OP-3011`)
* WebUI: The selected language will be saved now, even if the page is
  reloaded (:issue:`OP-3160`)

Fixed
~~~~~
* WebUI: added support for enabling EC overwrite on existing pools (:issue:`OP-3158`)
* WebUI: fixed typo in RBD creation form (:issue:`OP-3148`)
* WebUI: Ignore unused operations on the CRUSH Map Editor (:issue:`OP-3154`)
* WebUI: "Last Change" column not displayed in Pools table (:issue:`OP-3162`)
* WebUI: 404 not found when increasing number of rows displayed in a
  table (:issue:`OP-3163`)
* WebUI/QA: Fixed timing issue with scrub e2e tests (:issue:`OP-3152`)
* Backend: RGW proxy can't handle self-signed SSL certificates (:issue:`OP-3161`)
* Backend: REST API didn't respond in time (:issue:`OP-3164`)
