.. title: Ceph and Ceph Manager Dashboard presentations at openSUSE Conference 2018
.. slug: ceph-and-ceph-manager-dashboard-presentations-at-opensuse-conference-2018
.. date: 2018-05-28 14:56:17 UTC+02:00
.. tags: ceph, dashboard, conference, community, development, mgr, opensuse, osc18, suse
.. category: 
.. link: 
.. description: Sharing YouTube recordings of presentations by Laura and Kai at openSUSECon
.. type: text
.. author: Lenz Grimmer

Last weekend, the `openSUSE Conference 2018
<https://events.opensuse.org/conference/oSC18>`_ took place in Prague (Czech
Republic). Our team was present to talk about `Ceph <https://ceph.com>`_ and our
involvement in developing the Ceph manager dashboard, which will be available as
part of the upcoming Ceph "Mimic" release.

The presentations were held by `Laura Paduano
<https://twitter.com/Laura_Paduano_>`_ and `Kai Wagner
<https://twitter.com/ImTheKai>`_ from our team - thank you for your engagement!
The openSUSE conference team did an excellent job in streaming and recording
each session, and the resulting videos can already be viewed from `their YouTube
channel
<https://www.youtube.com/watch?v=OTPc36dVg8Q&list=PL_AMhvchzBafCf25yjL6Dix_moddfGfEb>`_.

`Ceph - The Distributed Storage Solution
<https://events.opensuse.org/conference/oSC18/program/proposal/1990>`_

* `Ceph Overview Slides <https://capri1989.github.io/osc-2018-ceph/>`_

.. youtube:: UtDNRX_Rd6U

`Ceph Manager Dashboard
<https://events.opensuse.org/conference/oSC18/program/proposal/1813>`_

* `Ceph Manager Dashboard Slides <https://capri1989.github.io/osc-2018/>`_

.. youtube:: 2UwRwJTDF0Q
