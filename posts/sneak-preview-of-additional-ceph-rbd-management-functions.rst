.. title: Sneak preview of additional Ceph RBD management functions
.. slug: sneak-preview-of-additional-ceph-rbd-management-functions
.. date: 2016-07-21 10:23:08 UTC+02:00
.. tags: ceph, rbd, management, preview, development
.. category:
.. link:
.. description: Showcasing an upcoming new Ceph management feature
.. type: text
.. author: Lenz Grimmer

The upcoming 2.0.13 release will see a number of new Ceph management and
monitoring features. While several of them aren't directly visible on the
WebUI (yet), Stephan has just `submitted a pull request
<https://bitbucket.org/openattic/openattic/pull-requests/351/rbd-ui-section-op-1290-op-753-op-1032-op/diff>`_
(to be merged shortly) that gives you access to functionality that Sebastian
(Wagner) added to the backend in version 2.0.12: this pull request will add
first Ceph RBD management capabilities.

This slide show gives you a preview of the upcoming changes:

.. slides::

  /galleries/oa-GUI-Ceph-RBD-2016-06/oa-ceph-rbd-list.png
  /galleries/oa-GUI-Ceph-RBD-2016-06/oa-ceph-rbd-create.png
  /galleries/oa-GUI-Ceph-RBD-2016-06/oa-ceph-rbd-delete.png
  /galleries/oa-GUI-Ceph-RBD-2016-06/oa-ceph-pool-list.png

.. TEASER_END

You will be able to sort the list of RBDs by additional criteria (e.g.
Poolname or Size), and you will now be able to create and delete RBDs in
openATTIC directly.

Moreover, Stephan's pull request adds some improvements to the visualization
of Pool details in the Ceph Pool list.

Once this pull request has been merged and all tests have passed, this
functionality will be available for testing in our nightly builds and the `live
demo system <https://demo.openattic.org/>`_.

As usual, we are grateful for any feedback and suggestions you may have!
