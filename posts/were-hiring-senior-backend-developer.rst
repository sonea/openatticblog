.. title: We're hiring: Senior Backend Developer
.. slug: were-hiring-senior-backend-developer
.. date: 2017-02-17 09:30:58 UTC+01:00
.. tags: opensource, development, collaboration, suse, hiring
.. category: 
.. link: 
.. description: Broadcasting the newly opened backend developer role
.. type: text
.. author: Lenz Grimmer

Last year, we opened up a position for :doc:`a senior frontend developer
<were-hiring-senior-frontend-developer>`. This position has now been filled and
we're very excited to welcome Ricardo Marques to our team! Ricardo just
concluded his first week and will support our team from out of his home office
near Lisbon, Portugal. He started `contributing
<https://bitbucket.org/ricardoasmarques/>`_ to openATTIC a while ago already,
and we look forward to his future contributions.

But we're still hiring; we've just opened a new position for the position of a
`Senior Backend Developer Enterprise Storage Management
<https://attachmatehr.silkroad.com/epostings/index.cfm?fuseaction=app.jobinfo&jobid=309012&source=ONLINE&JobOwner=1014654&company_id=15495&version=6>`_.

In this role, you'll be working with the openATTIC team on adding new features
to openATTIC's Python/Django-based backend code, as well as improving and
extending existing functionality.

See the job opening for further details on our expectations and requirements. If
you have any questions or would like to learn more, don't hesitate to `get in
touch <http://openattic.org/get-involved.html>`_ with us!

By the way, if you're interested in working on Linux and Open Source Software in
general, SUSE currently has `50+ job offerings
<https://www.suse.com/de-de/company/careers/>`_ available!