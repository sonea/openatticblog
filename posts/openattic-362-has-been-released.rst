.. title: openATTIC 3.6.2 has been released
.. slug: openattic-362-has-been-released
.. date: 2018-03-07 13:20:23 UTC+01:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release 
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.6.2 release
.. type: text
.. author: Sebastian Krah

We're happy to announce version 3.6.2 of openATTIC!

Version 3.6.2 is the second bugfix release of the 3.6 stable branch, containing fixes for multiple
issues that were reported by users.

One new feature that we want to point out is the internationalization. openATTIC has been
translated to Chinese and German to be present on other markets as well. 
We are working on other translations, for example Spanish. If you would like to see your native
language as part of openATTIC get in touch with us and we guide you how you can contribute and
help us with the translation.
We also had some packaging changes: Due to new requirements, we now use `_fillupdir` RPM macro in
our SUSE spec file.

As usual the release comes with several usability enhancements and security improvements.
For example we improved the modal deletion dialog in general - instead of just entering "yes"
when deleting an item, it is now required to enter the item name itself - so users do not
accidentally remove the wrong item.
Furthermore we fixed incorrect API endpoint URLs for RGW buckets. 
We also adapted/changed some test cases - e.g. e2e tests were converted into Angular unit tests.

.. TEASER_END

Your feedback, ideas and bug reports are very welcome. If you would like to
get in touch with us, consider joining our
`openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_
Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_
channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_
or leave comments below this blog post.

See the list below for a more detailed change log and further references. The
OP codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our
`public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.6.2
---------------------------

Added
~~~~~
* Backend: Add modifying the ``crush_rule`` of a pool from the API (:issue:`OP-3123`)
* Backend/QA: Increased unit test coverage of ceph app (:issue:`OP-2809`)
* WebUI: Validate hostname in settings page (:issue:`OP-2993`)
* WebUI: Add support for multiple languages (:issue:`OP-995`, :issue:`OP-3028`, :issue:`OP-3122`)
* WebUI: Add Ceph RBD Edit page (:issue:`OP-1033`, :issue:`OP-2764`)
* WebUI: Add Chinese and German translations (:issue:`OP-3104`)
* WebUI/QA: Added license header to all of our e2e files (:issue:`OP-1182`)
* Documentation: Added note about how to give the openattic user permissions to
  create a database (necessary to run the Python Unit tests) (:issue:`OP-2997`)

Changed
~~~~~~~
* WebUI/QA: Replaced some task queue e2e tests with unit tests (:issue:`OP-3023`)
* Backend: DeepSea min version for iSCSI should be 0.8.2 (:issue:`OP-3001`)
* Backend: Move oa_auth.py into REST directory (:issue:`OP-1437`)

Fixed
~~~~~
* Backend: RestClient class does not handle IPv6 addresses properly (:issue:`OP-2998`)
* Backend: RBD features should be enabled/disabled using the correct
  order (:issue:`OP-3019`)
* Backend: Set the RBD info 'block_name_prefix' (:issue:`OP-3027`)
* Backend: Increase timeout in MonApi::osd_pool_create (:issue:`OP-3017`)
* Backend: Fix URLs of RGW bucket endpoints (:issue:`OP-3140`)
* Backend: Handle invalid request URL's gracefully in RestClient (:issue:`OP-2983`)
* WebUI: Fix the pool deletion pool listing (:issue:`OP-2980`)
* WebUI: Fix error thrown when switching from RBD Add page (:issue:`OP-2986`)
* WebUI: Pool names are now displayed in the Pool Deletion dialog (:issue:`OP-3024`)
* WebUI/QA: Fix random error in 'general' e2e tests (:issue:`OP-3013`)
* WebUI/QA: Fix random e2e error regarding number of pending tasks (:issue:`OP-3018`)
* WebUI/QA: Fix Ceph Pools e2e test (:issue:`OP-2967`)
* WebUI/QA: Fix check current user name (:issue:`OP-3005`)
* WebUI/QA: Fix Ceph NFS "Should remove export" e2e test (:issue:`OP-2714`, :issue:`OP-3015`,
  :issue:`OP-3016`)
* WebUI/QA: Wait until modal dialog is invisible to fix a timing
  issue (:issue:`OP-2994`)
* WebUI/QA: Set chrome for e2e testing to US (:issue:`OP-3142`)
* WebUI/QA: Test task queue deletion solely by unit tests (:issue:`OP-3034`)
* Packaging: Use `_fillupdir` RPM macro in SUSE spec file (:issue:`OP-3100`)