.. title: Sneak preview: Upcoming Ceph Management Features
.. slug: sneak-preview-upcoming-ceph-management-features
.. date: 2017-05-05 12:36:38 UTC+02:00
.. tags: ceph, development, iSCSI, management, preview
.. category:
.. link:
.. description: Previewing the iSCSI and RGW Management functionality
.. type: text
.. author: Lenz Grimmer

Despite the number of disruptive changes that we went through in the past few
weeks, e.g. :doc:`moving our code base from Mercurial to git
<openattic-code-repository-migrated-to-git>`, relocating our infrastructure to a
new data center, :doc:`refactoring our code base for version 3.0
<implementing-a-more-scalable-storage-management-framework-in-openattic-30>`,
our developers have been busy working on expanding the Ceph management
capabilities in openATTIC.

I'd like to highlight two of them that are nearing completion and should land
in the ``master`` branch shortly.

.. TEASER_END

A very significant new feature is the management and configuration of iSCSI
targets, which is tracked in :issue:`OP-775`. This feature uses Ceph RADOS block
devices (RBDs) as the backing store and `lrbd <https://github.com/SUSE/lrbd>`_
for the target configuration on the iSCSI storage nodes; which in turn is
orchestrated by our Salt-based Ceph deployment and configuration framework
`DeepSea <https://github.com/SUSE/DeepSea>`_.

This work consists of two main tasks: implementing the required openATTIC
backend functionality (:issue:`OP-2133`), which establishes a communication path
between openATTIC and the Salt master using the Salt REST API. This part was
developed by Ricardo Dias; the pull request can be reviewed here: `PR#88
<https://bitbucket.org/openattic/openattic/pull-requests/88/op-2133-ceph-iscsi-targets-management/diff>`_.

Having a generic REST API route to Salt/DeepSea allows us to add more management
functionality to openATTIC more easily, which is then automated and coordinated
by Salt. As an admistrator, you are free to use either the Salt CLI, or perform
the same task via the openATTIC UI.

The corresponding iSCSI management frontend work is tracked in :issue:`OP-2134`,
which is developed by our newest member of the team, Ricardo Marques. With this,
you will be able to add/edit/clone and delete iSCSI targets, as well as starting
or stopping them. The pull request related to this part is `PR#73
<https://bitbucket.org/openattic/openattic/pull-requests/73/op-2134-ceph-iscsi-targets-management/diff>`_.

Here's a preliminary set of screen shots:

.. slides::

    /galleries/oa-Ceph-iSCSI-preview-2017-05-05/Screenshot_20170505_133428.png
    /galleries/oa-Ceph-iSCSI-preview-2017-05-05/Screenshot_20170505_133621.png
    /galleries/oa-Ceph-iSCSI-preview-2017-05-05/Screenshot_20170505_133923.png
    /galleries/oa-Ceph-iSCSI-preview-2017-05-05/Screenshot_20170505_134032.png
    /galleries/oa-Ceph-iSCSI-preview-2017-05-05/Screenshot_20170505_134115.png
    /galleries/oa-Ceph-iSCSI-preview-2017-05-05/Screenshot_20170505_134205.png
    /galleries/oa-Ceph-iSCSI-preview-2017-05-05/Screenshot_20170505_134320.png

Next up, I'd like to highlight the ongoing work related to managing Ceph RADOS
Gateway objects like users, subusers and their related keys and capabilities.

Similar to the iSCSI work, this feature consists of two separate tasks. On the
one hand, we needed to establish a connection from openATTIC to the `RGW Admin
Ops API <http://docs.ceph.com/docs/master/radosgw/adminops/>`_, which is
developed by Patrick Nawracay and tracked in :issue:`OP-1612` (`PR#72
<https://bitbucket.org/openattic/openattic/pull-requests/72/wip-establish-backend-interface-to-the/diff>`_).

The web frontend work is performed by Volker Theile (:issue:`OP-762`). With this
feature, you will be able to Add/Edit/Remove RGW users and subusers, as well as
adding/editing their access keys and capabilities. There is no pull request for
this part yet, please leave any feedback on the Jira issue for the time being.

.. slides::

    /galleries/oa-Ceph-RGW-Management-preview-2017-05-05/cap.png
    /galleries/oa-Ceph-RGW-Management-preview-2017-05-05/s3_key.png
    /galleries/oa-Ceph-RGW-Management-preview-2017-05-05/subuser.png
    /galleries/oa-Ceph-RGW-Management-preview-2017-05-05/swift_key.png
    /galleries/oa-Ceph-RGW-Management-preview-2017-05-05/user.png

Please see the Jira issues for additional and updated screen shots and mockups.

We hope you like the direction in which we're heading with these features! If
you have any feedback or comments, please leave them in the pull requests or
corresponding Jira issues, or :doc:`get in touch with us <get-involved>` via our
Google Group.