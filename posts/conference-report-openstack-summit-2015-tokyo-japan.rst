.. title: Conference report: OpenStack Summit 2015 Tokyo, Japan
.. slug: conference-report-openstack-summit-2015-tokyo-japan
.. date: 2015-11-13 10:32:43 UTC+01:00
.. tags: conference, event, openstack, summit, tokyo, openattic, storage, ceph, cinder, swift
.. category:
.. link:
.. description: A summary of my visit to the OpenStack Summit 2015 in Tokyo, Japan
.. type: text
.. author: Lenz Grimmer

The `OpenStack Foundation <https://www.openstack.org/>`_ held their last
biannual `OpenStack Summit <https://www.openstack.org/summit/tokyo-2015/>`_ in
Tokyo, Japan, from October 27-30, 2015. OpenStack Summits include keynotes,
in-depth technical discussions, hands-on workshops, and the full presence of
almost every key player in the OpenStack ecosystem.

I attended the summit together with three other colleagues from it-novum, from
Tuesday till Friday. In this article, I'll try to summarize my impressions and
share my notes and observations.

.. TEASER_END

The Tokyo summit took place in the Grand Prince International Convention Center
& Hotels, which was easy to reach via public transport. The conference venue
was spread across several buildings and floors, which sometimes made it
difficult to navigate and find the right room. Fortunately, friendly helpers
were present everywhere, to provide guidance and directions. Overall, the
conference was organized in a very professional manner and it was a pleasure to
attend.

My focus was on attending storage-related sessions as well as presentations
about Linux Containers (e.g. Docker) and other OpenStack-related tools for
automation and deployment. I attended mostly sessions from the main conference,
but also jumped over to some sessions in the Design Summit from time to time,
which was held in parallel to the main conference in a separate building. The
schedule was quite intense - I quickly realized that there were sometimes 3-4
sessions taking place in parallel that I would have loved to attend!

Between the larger breaks, there were only 5 minute breaks between the session
slots, which sometimes made it difficult to reach the next session in time.
Moreover, some rooms were fairly small and more than once the rooms were
overflowing by the time I reached the entrance. Fortunately, most of the
sessions were recorded on video and were `made available
<https://www.openstack.org/summit/tokyo-2015/videos/>`_ within hours after the
presentation was held .  The availability of video recordings somewhat eased
the pain of not being able to attend multiple sessions of interest that took
place at the same time.

In addition to the ongoing sessions, there was the "OpenStack Marketplace", a
large exhibition floor located in the basement of a building next to the
convention center. The marketplace was somewhat remote, but all of the key
players/vendors in the OpenStack ecosystem (SUSE, Canonical and Red Hat,
Mirantis and hardware vendors of all brands) were present. I personally enjoyed
meeting the founders of `Quobyte <http://quobyte.com/>`_ from Berlin, to learn
more about their interesting distributed storage system, which is based on
their former research project "`XtreemFS <http://xtreemfs.org/>`_".

To keep track of the schedule, the mobile App was invaluable. However its
usability and performance left a lot to be desired: it did not provide an
offline mode and did not take the current time and date into consideration,
which resulted in a lot of unneccesary scrolling.

The social event on Wednesday night also was a memorable event. This year, the
key sponsors collaborated on organizing one big party instead of organizing
several separate ones. The event took place in the "`Happo-En
<http://www.happo-en.com/english/>`_", a beautiful Japanese garden with a pond.
In addition to a wide variety of food, it provided lots of activities and
performances: sumo wrestling, geisha performances, Taiko drum lessons, sake
tasting, Cosplay...

The entire event had a strong focus on community and collaboration. The term
"We are OpenStack" and the #weareopenstack hash tag were promoted throughout
the conference, to share moments and highlights with other participants.

Another thing that impressed me was the availability of simultaneous
translations for sessions held in Japanese. Attendees could obtain headphones
when entering the room, to listen to these presentations in English.

Notes and takeaways
~~~~~~~~~~~~~~~~~~~

Tuesday and Wednesday started with keynotes, that were also broadcast in
additional "overflow rooms", to accommodate the large number of attendees that
could not fit into the main auditorium. Some personal highlights from the
various keynotes I attended include:

* The `OpenStack Project Navigator
  <http://www.openstack.org/software/project-navigator/>`_, to help maintaining
  an overview about the adoption, age and maturity of the various OpenStack
  subprojects. This will help new users to obtain some guidance on what projects
  are safe to use, and which should be used with caution.
* The fact that Software defined Networking (SDN) grows twice as fast as server
  virtualization.
* Erica Brescia's (COO BitNami) keynote `Banishing the Shadow Cloud
  <https://youtu.be/aCC1zTfYXjo>`_. This was a thought-provoking and insightful
  presentation, while most other sponsored keynotes were somewhat too product
  centric for my taste.

Below is a (likely incomplete) list of sessions I attended, including some
notes and key takeaways. I've also taken some pictures during the Summit, which
can be found in my `OpenStack Summit Flickr Set
<https://www.flickr.com/photos/lenzgr/albums/72157660336371619>`_.

I also managed to sneak in some time for sightseeing, you can take a look at my pictures of Tokio
`here <https://www.flickr.com/photos/lenzgr/albums/72157658756557383>`_.

`Building web-applications using OpenStack Swift
<https://openstacksummitoctober2015tokyo.sched.org/event/37ef34c4becca0e0416cd08d1b90c3e7>`_
by Christian Schwede (Video: https://youtu.be/4bhdqtLLCiM)

This session was a very good introduction into the topic of "object storage" in
general, giving an overview about the general concepts and access methods and
then covering OpenStack Swift in more detail from a developer's perspective.
Christian then built a simple web application that used Swift to store the
actual data to provide a real-world use case, followed by some "Do's and
Don'ts" best practice advice.

`What Should I Know About?
<https://openstacksummitoctober2015tokyo.sched.org/event/85e907c98e188324ba6593d4d4ee4652>`_
by Mike Metral (Video: https://youtu.be/jB3pi2knSFM)

In this fast-paced session, Mike gave a speed-dating type overview about
various Linux container technologies, related tools and frameworks that help to
manage and orchestrate large-scale container deployments. It covered a lot of
technologies just briefly, so this was a useful session if one had some general
knowledge of containers already and just wanted to get a broader overview of
the container ecosystem. However, there was no real conclusion except there is
a lot of choice, so choosing the most appropriate tool for the job at hand is
still up to the user. This session gave me a lot of pointers and hints about
what tools might be worth evaluating, but it also left me somewhat puzzled and
overwhelmed by the sheer amount of choices that were presented.

`Life Without DevStack: Upstream Development With OSAD
<https://openstacksummitoctober2015tokyo.sched.org/event/bc52760348c5080fe35cda5fb8571856>`_
by Miguel Grinberg (Video: https://youtu.be/OSkGaxR1yds)

In this talk, Miguel talked about the pains and problems that he was facing
when using DevStack for performing local development on certain OpenStack
projects. The proposed alternative "OpenStack Ansible Deployment (OSAD)
distribution" (especially the bootstrap-aio.sh script) looked quite promising:
instead of installing all OpenStack components on a sinlge node, this script
puts every OpenStack component in a dedicated LXC container, using Ansible for
the assembly and configuration. If you're an OpenStack developer, this tool
might come in very handy. He also covered this project in a `detailed blog post
<https://developer.rackspace.com/blog/life-without-devstack-openstack-development-with-osa/>`_.

`The Comparison of Ceph and Commercial Server SAN
<https://openstacksummitoctober2015tokyo.sched.org/event/9dc3b9e201127dd4d57e27fcbf4f0346>`_
by Yuting Wu (Video: https://youtu.be/fDs2sdl9VnY)

Yuting Woo from AWCloud summarized his findings comparing Ceph​ with
proprietary solutions from Solidfire and ScaleIO in terms of storage
functionality, deployment options, operations and maintenance as well as
performance. Based on his findings, there is still a lot of room for
improvement in Ceph, particularly in the areas of deployment, management,
performance as well as advanced storage features like QoS (Quality of Service),
Deduplication or Compression.

`A Conversation With Cinder Developers
<https://openstacksummitoctober2015tokyo.sched.org/event/b2911465845bf1790a7eafba468200bf>`_
by Jay Bryant, Patrick East, John Griffith, Zhiteng Huang, Sean McGinnis, Mike
Perez, Xing Yang (Video: https://youtu.be/E86wtu_yMPk)

The Cinder core developers shared their view of the state of Cinder and where
the project is heading. API versioning was mentioned as an issue, and API
microversioning was discussed as a solution. Other tasks under development
include: performing backups of snapshots instead of volumes, as well as
implementing TRIM support. The Cinder developers are also contemplating to
decouple Cinder from OpenStack, to convert it into a standalone, general
purpose service for other cloud technologies other than OpenStack.

`Distros: Let's Not Make *That* Mistake Again
<https://openstacksummitoctober2015tokyo.sched.org/event/e150d6883fd36ee66c5b4820864e56a8>`_
by Jesse Proudman (Video: https://youtu.be/FhHB83aPzZU)

In this session, Jesse argued if having multiple distributions (or "flavors")
of OpenStack is really helping the project or actually harming it. He also
asked what OpenStack users really want in order to get started with it. He
observed a trend that many users would like to start using OpenStack, without
actually having to setup and deploy the infrastructure by themselves. He sees
this as a market opportunity for companies, to set up and manage OpenStack
infrastructures on behalf of their clients, in their own data centers.

`Walk Through a Software Defined Everything PoC
<https://openstacksummitoctober2015tokyo.sched.org/event/0d803ff02763a7663519d3934f8ae120>`_
by Chris Janiszewski, Sandro Mathys

This presentation talked about a proof-of-concept (POC) installation of
OpenStack, using Ceph as the storage backend and `MidoNet
<https://www.midonet.org/>`_ as the network virtualization layer used by
Neutron. The speakers shared their experiences and key learnings from this
project. One interesting insight was their choice of using `xCat
<http://xcat.sf.net/>`_ (Extreme Cluster/Cloud Administration Toolkit) for the
provisioning and management of the OpenStack nodes themselves.

`Ceph Community Talk on High-Performance Solid State Ceph
<https://openstacksummitoctober2015tokyo.sched.org/event/a4911f6555ee768756498aa27bc6e4e0>`_
by Reddy Chagam, Gunna Marripudi, Allen Samuels, Warren Wang

In this session, representatives from various storage companies (Intel,
Samsung, SanDisk) talked about the projects their are working on to improve
Ceph's performance on solid state devices. A lot of effort is being put into
creating benchmarks and other profiling and testing tools. For example, Intel
has worked with Red Hat to release the `Ceph Benchmarking Toolkit
<https://github.com/ceph/cbt>`_ (CBT) as open source. SanDisk is also putting a
lot of effort into improving Ceph's performance, by submitting patches that
make the entire code base more scalable and to resolve some of the bottlenecks
they observed. In general, some of the assumptions about spinning disks that
influenced design decisions in Ceph are now being put on the table for
re-examination and possible ways of improvement. This talk was very informative
and reinforced the impression, that Ceph's existing performance deficiencies
are goint to be adressed over time.

`New Ceph Configurations - High Performance Without High Costs
<https://openstacksummitoctober2015tokyo.sched.org/event/c38609563db7791cb3b7e46777256688>`_
by Allen Samuels

Allen Samuels presented some recent performance improvements and shared some
benchmark results that SanDisk has been working on to improve Ceph's
performance. He also compared the use of erasure coding versus traditional
replication and cache/tiered storage pools in combination with flash devices.

`Ansible Collaboration Day: Ansible + OpenStack — State of the Universe
<https://openstacksummitoctober2015tokyo.sched.org/event/2e552ffdfb274a8310e8b8b950bc2108>`_
by Robyn Bergeron, Mark McLoughlin

This was an open meetup of Ansible users and representatives from Red Hat (who
acquired Ansible a few weeks before the OpenStack Summit). It included a
presentation of upcoming features in Ansible 2.0, which is highly anticipated
by the community and will include a number of useful improvements. The release
seems to be imminent, but no firm date was given. For the Ansible team, not
much semms to have changed after the acquisition. The question if Ansible Tower
will be released under an open source license was raised, and the expectation
is that the Ansible acquisition will be no differerent than other acquisitions
in terms of what happens to previously proprietary components (e.g. Ceph's
Calamari, which was released as open source after Red Hat acquired InkTank).

`Turning Pets Into Cattle: A Demonstration to Provoke Discussion
<https://openstacksummitoctober2015tokyo.sched.org/event/170731226edd33f258bb7ae140d1b2cf>`_
by Yih Leong Sun, Stephen Walli (Video: https://youtu.be/wL9FOHKpmCU)

In this session, Stephen and Leong discussed some of the aspects of moving
existing applications from dedicated servers into a cloud enviroment. Even
though this session was intended to be "non-technical", it contained a live
demo in which the web tier of a WordPress application was migrated into the
cloud, while keeping the database service on premise. In a following step,
static content was pushed into a Swift object store. I found the mix of
high-level business aspects with a live shell demo somewhat confusing, even
though the session itself contained a few insightful recommendations and
insights.

`Kolla: Ansible Deployment + OpenStack in Docker Containers = Operator Bliss
<https://openstacksummitoctober2015tokyo.sched.org/event/01319031bc2b6c9c4d1ccc24647b2f96>`_
Steven Dake, Daneyon Hansen, Sam Yaple (Video: https://youtu.be/BKYJuYsT4z4)

Steven and Sam introduced `Kolla <https://github.com/openstack/kolla>`_, an
OpenStack deployment tool based on Ansible and Docker that supports a wide
range of Linux distributions (RHEL and derivatives like CentOS or Oracle Linux,
Ubuntu Linux). The session included a live demo to explain Kolla's
functionality, that deployed a functional OpenStack environment (including
Ceph) in about 20 minutes. Kolla is developed by a diverse community, including
engineers from Red Hat, Cisco Systems, Oracle and Rack Space. It was
interesting to learn that this is not only used for setting up OpenStack
development environments, but also for real-life production environments as
well. The development is guided and steered by the community and is designed
for scale and openness, including a thorough review and testing process.

`Persisting Data In Your Cloud With Cinder Block Storage
<https://openstacksummitoctober2015tokyo.sched.org/event/7d49f1a7926da2f2b28eea1f19cac740>`_
by John Griffith, Kenneth Hui, Arun Sriraman (Video: https://youtu.be/qeFz0pwVO6c)

This session started as a basic introduction into Cinder and the benefits of
using OpenStack as a developer, but quickly turned into a product showcase for
Platform9 and SolidFire. For someone new to Cinder, the first part of the
session was quite useful to get up to speed, the second part showed a real-life
production configuration that could be used as a blueprint for own deployments.

`Finally FDE - OpenStack Full Disk Encryption and Missing Pieces
<https://openstacksummitoctober2015tokyo.sched.org/event/28b41d7e3875f45e8ab8f95bd1afdeb6>`_
by Robert Clark, Dave McCowan, Arvind Tiwari (Video: https://youtu.be/phMUoq9OYHY)

One of the key issues with encrypted disk drives is the management of the
encryption keys. In an OpenStack environment, it is very impractical to wait
for the administrator to log into the console and enter the decryption
passphrase every time a virtual machine reboots. In this session, Robert,
Arvind and Dave gave an overview about the state of encryption in various
OpenStack projects and also introduced `Project Marshal
<https://github.com/openstack/marshal>`_ and `Project Leeson
<https://github.com/hyakuhei/Leeson>`_. The latter implements a key broker
service based on the UUID of the guest OS. Securing data in a cloud environment
is a big challenge, this session provided a good overview and some practical
examples on how data could be encrypted without too much manual effort.

`Ceph and OpenStack: Current Integration and Roadmap
<https://openstacksummitoctober2015tokyo.sched.org/event/042148c9d048a2907c3a48e5fc139f50>`_
by Josh Durgin, Sébastien Han (Video: https://youtu.be/5Wqb5sZG11I)

Josh and Sébastien from Red Hat gave an overview and status update on Ceph and
OpenStack support. They started by giving a general overview about Ceph and its
components, followed by an introduction of how Ceph integrates with OpenStack,
e.g. as a backend for Keystone (using the Swift API support provided by RADOS
Gateway), Cinder, Glance and Nova. A lot of bugs in the interoperabilty with
Ceph​ were fixed in the OpenStack "Liberty" release. New features like
Cinder volume migration were also introduced in this release cycle.

The Ceph "Infernalis" release is also close to being released, which will
include new features like per-image metadata, flattening of snapshots and
easier deletion of parent images. The "Jewel" Ceph release will also include
many highly desired features. For example, CephFS might actually be declared
stable in Q1 2016 - according to the developers, the lack of a functional file
system check tool (fsck) is one of the main remaining road blocks. Write
performance improvements are also much appreciated.

This session was packed with news and interesting developments. If you're
interested in where Ceph and OpenStack are heading, this session was a real
gem.

`Beginners Guide to Containers Technology and How it Actually Works
<https://openstacksummitoctober2015tokyo.sched.org/event/34d802ecf579932fd67b9ff033360d10>`_
James Bottomley (Video: https://youtu.be/YsYzMPptB-k)

This was the most thorough and in-depth presentation about Linux containers I
have ever attended, going down to the actual Linux kernel APIs that provide the
foundation for Linux containers (regardless of their actual implementation,
e.g. Docker, LXC or LXD). After explaining the general benefits of using
containers over virtual machines (VMs), James did an excellent job of
explaining and demonstrating the various low-level systems, e.g. name spaces or
control groups. Unfortunately he ran out of time and had to rush through the
rest of the presentation. Also worth mentioning was that the presentation was
created using the `Impress.JS <https://github.com/impress/impress.js>`_
presentation framework, which provided a very welcome change to the many "death
by PowerPoint" presentations.

`Manila - An Update From Liberty
<https://openstacksummitoctober2015tokyo.sched.org/event/5863eb14707e707a54f982e7aac966aa>`_
by Thomas Bechtold, Sean Cohen, Akshai Parthasarathy (Video: https://youtu.be/cVMSUl0oago)

This was a joint presentation by representatives from NetApp, Red Hat and SUSE
about the OpenStack Shared File Service "Manila". It started by giving an
overview of Manila, its use cases and what's new in the OpenStack "Liberty"
release. The representatives from SUSE and Red Hat then talked about the status
of manila on their distributions. SUSE aims to fully support Manila with SUSE
Cloud 6, Red Hat is working on CephFS support (using NFS-Ganesha to export file
systems via NFS). They concluded the presentation with an outlook of what to
expect from the upcoming "Mitaka" release and a live demonstration of the
"share replication" feature, which allows creating high available NFS shares
(if the backend supports it).

Closing thoughts
~~~~~~~~~~~~~~~~

To summarize my personal impressions and key learnings as a first-time
attendee: OpenStack Summit is huge! Don't even try to attend all the sessions
that interest you, otherwise it will be a frustrating experience. Plan your
sessions in advance, and also plan to allocate some time after the event, to
watch the recordings of sessions you were not able to attend. Also, take notes!
Otherwise the sheer amount of information you have to digest in such a short
period of time will overwhelm and confuse you.

For more hints, the session `Proud to be a Noob: How to Make the Most of Your
First OpenStack Summit <https://youtu.be/I34ce_6vl5s>`_ is probably a useful
resource. Unfortunately I missed this one...

In addition to learning a lot about recent developments and ongoing activities
in the OpenStack ecosystem, the networking and meeting people behind the
projects was probably the most valuable benefit for me. This kind of
interaction was only possible by attending the Summit in person, instead of
watching the video recordings offline. I look forward to attending the next!
