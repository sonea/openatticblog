.. title: openATTIC 3.4.1 has been released
.. slug: openattic-341-has-been-released
.. date: 2017-07-21 14:32:34 UTC
.. tags: announcement, ceph, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the openATTIC 3.4.1 release
.. type: text
.. author: Sebastian Krah

We are very happy to announce the release of openATTIC version 3.4.1
In this version we completely removed Nagios/PNP4Nagios graphs from the UI and installation in favor of Prometheus/Grafana. 

We've continued with the integration of Ceph Luminous features. The 'allow_ec_overwrites' flag can now be set when creating erasure coded pools via the REST API. The UI part is currently under construction.
Enabling the features "layering" and "striping" at once is also supported when creating an RBD now. Furthermore support for the new 'ceph health' format has been integrated.

The UI settings page has been extended to support the configuration of Grafana and gracefully handle a not properly entered config - which means it's no longer needed to set this configuration in /etc/sysconfig/openattic or /etc/default/openattic.
The Salt-API could now be configured by using sharedsecret-key authentication, in addition to 'auto'.
As usual we also improved some exsting UI features, this release, for example, contains help-text changes to provide users with more troubleshooting hints for possible solution.

.. TEASER_END

During the final installation tests of openATTIC 3.4.0 we discovered a bug caused by the removal of Nagios, which was fixed in v3.4.1 immediately.

We would like to thank everyone who contributed to this release.

Your feedback, ideas and bug reports are very welcome. If you would like to get in touch with us, consider joining our `openATTIC Users <https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group, visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on `irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave comments below this blog post.

See the list below for a more detailed change log and further references. The OP codes in brackets refer to individual Jira issues that provide additional details on each item. You can review these on our `public Jira instance <http://tracker.openattic.org/>`_.

Changelog for version 3.4.1
----------------------------

Added
~~~~~
* Backend: Removed all signals from the nagios module in preparation for the complete removal (:issue:`OP-2500`)

Changelog for version 3.4.0
----------------------------

Added
~~~~~
* Backend: The 'allow_ec_overwrites' flag will now be recognized by creating an erasure coded pool (:issue:`OP-2356`)
* Backend: OSD list is now filterable by 'osd_objectstore' (:issue:`OP-2417`)
* Backend: Automatically reload Grafana settings after openattic config file changed (:issue:`OP-2426`)
* Backend/WebUI: Added support for Salt-API "sharedsecret" eauth authentication (:issue:`OP-2451`)
* Backend: Added `stripe_unit` and `stripe_count` to the RBD management API (:issue:`OP-2236`)
* WebUI: Add a menu to navigate to the users buckets (:issue:`OP-2382`)
* Backend: Added Grafana configuration to the settings REST service (:issue:`OP-2450`)
* Backend: Returns the status of the Grafana integration through the API (:issue:`OP-2438`)
* Backend: Added the functionality to filter the Ceph pool list by the enabled flags of a pool (:issue:`OP-2446`)
* Backend: Added REST service to check Grafana connection (:issue:`OP-2449`)
* Backend: Add support to override settings via backend/settings_local.conf (:issue:`OP-2473`)
* WebUI: Make Grafana configurable from the UI (:issue:`OP-2427`)
* WebUI: Gracefully handle not properly configured Grafana (:issue:`OP-2434`)
* Backend: Add time-sync-status to the API (:issue:`OP-2495`)
* WebUI: Display help text for shared secret key configuration in settings page (:issue:`OP-2479`)

Changed
~~~~~~~
* WebUI: Refactors add form template (:issue:`OP-2420`)
* WebUI: Adapted the paths to grafana dashboards to the latest deepsea changes (:issue:`OP-2442`)
* WebUI: Rename "Clone" button to "Copy" (:issue:`OP-2478`)
* WebUI: iSCSI - Rename "Target" to "Target IQN" (:issue:`OP-2481`)
* Backend: Cope with the new Ceph health format (:issue:`OP-2489`)
* WebUI: Add support for new Luminous Ceph health format (:issue:`OP-2471`)
* WebUI: Enhance troubleshooting hints when Salt REST API is not reachable (:issue:`OP-2492`)
* Backend: Hide the controls of the Grafana graphs for OSDs, Nodes and Pools (:issue:`OP-2423`)

Removed
~~~~~~~
* Installation: Removed Nagios and PNP4Nagios from the RPM package and removed configuration from `oaconfig` (:issue:`OP-2267)`
* Backend: All Nagios relations have been removed from the Ceph module (:issue:`OP-2490`)
* Backend: Removed unused ceph/systemapi.py (:issue:`OP-2494`)
* WebUI: Removed all Nagios relations from UI and e2e test (:issue:`OP-2448`)

Fixed
~~~~~
* Backend: The status module returns "available" for modules missing the status.py file. (:issue:`OP-2441`)
* Backend: The status module returns JSON if a module couldn't be found by now. (:issue:`OP-2440`)
* WebUI: Remove unique name errors in pool form (:issue:`OP-2421`)
* Development: Add cephfs.so to Vagrant's virtual environment (:issue:`OP-2463`)
* Development: Fix various issues in the Vagrant box installer script.
* Backend: Commented settings should not be removed by openATTIC (:issue:`OP-2459`)
* WebUI: Grafana loading spinner will be displayed as well as the icon in the notification list now (:issue:`OP-2431`)
* Backend: Handle JSON decode exception in REST Client class (OP-2476`)
* Backend/WebUI: Fixed the usage/configuration of the RGW_API_RESOURCE setting (:issue:`OP-2415`)
* Backend/WebUI: Fixed check of RGW system user permissions in RGW status module (:issue:`OP-2416`)
* Backend: Fixed RGW system user permissions check in NFS module (:issue:`OP-2465`)
* Backend: Initialize all Nodb fields with their defaults (:issue:`OP-2279`)
* WebUI: Grafana - Add cancellation of resize interval (:issue:`OP-2482`)
* WebUI: The min-height of the grafana iframe is 600px now. So, the iframe isn't too small anymore if the dashboard couldn't be loaded (:issue:`OP-2486`)
* WebUI: Fixed settings page if RGW config is not available in DeepSea (:issue:`OP-2433`)
* WebUI: Fixed spacing between button icons and text (:issue:`OP-2452`)
