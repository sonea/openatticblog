.. title: IMAP: Sync all Folders in Thunderbird
.. slug: imap-sync-all-folders-in-thunderbird
.. date: 2017-03-07 11:03:01 UTC+01:00
.. tags: imap sync thunderbird  
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

I could be very annoying to use Thunderbird with multiple folders. Everytime you'll create a new subfolder you have to change the properties and click the checkbox to download all emails to this folder without the need to click on this specific folder every time. 

I found a global option which made my life much easier. Just use the config editor (about:config) and change the following entry::

	mail.server.default.check_all_folders_for_new -> true

That's it. 
