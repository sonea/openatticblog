.. title: Contributing Patches and Reporting Bugs
.. slug: contributing-patches-and-reporting-bugs
.. date: 2015-12-11 10:48:27 UTC+01:00
.. tags: announcement, collaboration, contributing, opensource
.. category:
.. link:
.. description: Introducing changes to accepting patch contributions and reporting bugs
.. type: text
.. author: Lenz Grimmer

From very early on (Feb 2011), openATTIC has been licensed under the `GNU
General Public License (GPLv2) <https://www.gnu.org/licenses/#GPL>`_ and new
releases were made publicly available for `download
<http://download.openattic.org>`_.

However, contributing patches or submitting code for new features was somewhat
of an ordeal, as it required filling out and submitting a "`Contributor
License Agreement
<https://en.wikipedia.org/wiki/Contributor_License_Agreement>`_". This was
mandatory, as we wanted to maintain the option to relicense openATTIC under a
different license.

Going forward, this is no longer a requirement. Instead, we have decided to
adopt the "sign-off" procedure `introduced by the Linux kernel
<http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/Documentation/SubmittingPatches>`_.

.. TEASER_END

The sign-off is a simple line at the end of the explanation for the commit or
pull request, which certifies that you wrote it or otherwise have the right to
pass it on as an open-source patch. We have documented the exact process in
the `openATTIC Contributing Guidelines
<http://docs.openattic.org/2.0/developer_docs/contributing_guidelines.html#signing-your-patch-contribution>`_.

Unfortunately, unlike ``git``, the Mercurial DVCS does not provide a built-in
option to add the required ``Signed-off-by`` line below a commit message
automatically. Therefore, we created a simple `Mercurial signoff hook
<https://bitbucket.org/snippets/LenzGr/aygXb>`_ that you can install into the
``.hg`` directory of your local openATTIC repository (see the comment header
for installation instructions).

Hopefully this will further lower the hurdle for external developers for
submitting patches or contributing code.

If you're looking for interesting tasks that could be tackled, take a look at
our `public openATTIC Jira instance <http://tracker.openattic.org/>`_. We use
this instance to coordinate all our work and to collect open issues and tasks.

The openATTIC Jira tracker will replace the `current bug tracker on BitBucket
<https://bitbucket.org/openattic/openattic/issues?status=new&status=open>`_.

So if you want to report a new bug or comment on an existing issue, either
`sign up <http://tracker.openattic.org/secure/Signup!default.jspa>`_ for a new
account, or use your existing Facebook, GitHub or Google account for
authentication.

We are looking forward to your feedback and suggestions!
