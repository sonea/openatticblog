.. title: Developing with Ceph using Docker
.. slug: developing-with-ceph-using-docker
.. date: 2016-10-28 15:39:19 UTC+02:00
.. tags: development, ceph, docker, linux, opensource, testing
.. category:
.. link:
.. description: How to run Ceph in a Docker container for development purposes
.. type: text
.. author: Lenz Grimmer

As you're probably aware, we're putting a lot of effort into improving the Ceph
management and monitoring capabilities of openATTIC in collaboration with SUSE.

One of the challenges here is that Ceph is a distributed system, usually running
on a number of independent nodes/hosts. This can be somewhat of a challenge for
a developer who just wants to "talk" to a Ceph cluster without actually having
to fully set up and manage it.

Of course, you could be using tools like SUSE's Salt-based `DeepSea
<https://github.com/SUSE/DeepSea>`_ project or `ceph-ansible
<https://github.com/ceph/ceph-ansible>`_, which automate the deployment and
configuration of an entire Ceph cluster to a high degree. But that still
requires setting up multiple (virtual) machines, which could be a daunting or at
least resource-intensive task for a developer.

While we do have a number of internal Ceph clusters in our data center that we
can use for testing and development purposes, sometimes it's sufficient to have
something that behaves like a Ceph cluster from an API perspective, but must not
necessarily perform like a full-blown distributed system (and can be set up
locally).

Fortunately, `Docker <https://www.docker.com/>`_ comes to the rescue here - the
nice folks at Ceph kindly provide a special Docker image labeled `ceph/demo
<https://hub.docker.com/r/ceph/demo/>`_, which can be described as a "Ceph
cluster in a box".

.. TEASER_END

From the description: *The main purpose of this container is to quickly get a
Ceph cluster up and running by reducing all the setup steps. The container
provides all the Ceph daemons, so you can rapidly start playing with Ceph.*

Note that this container is not suitable for production purposes by any means,
but it's the ideal lightweight solution for developers who want to create
applications that need to communicate with a "real" Ceph cluster.

For openATTIC development purposes, enabling Ceph support is basically a
one-liner (assuming that Docker is installed and running on your openATTIC
development system already)::

    # docker run -d --net=host -v /etc/ceph:/etc/ceph -e MON_IP=192.168.178.92 \
      -e CEPH_NETWORK=192.168.178.0/24 -e CEPH_PUBLIC_NETWORK=192.168.178.0/24 \
      ceph/demo

This command will start a single Docker container which runs a minimal Ceph
cluster, including a Monitor, **one** OSD and RADOS Gateway. Yes, this is a
pathological case, but it's good enough for doing Ceph development.

You need to adapt the IP and network addresses for ``MON_IP``, ``CEPH_NETWORK``
and ``CEPH_PUBLIC_NETWORK`` according to your environment. In this case, I've
used my openATTIC development system's public IP address as the as the monitor
address. Also keep in mind that this makes the Ceph instance accessible from the
public network!

After a short initialization phase, the cluster is up and running::

  # docker ps -a
  CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
  3a2a372b855a        ceph/demo           "/entrypoint.sh"    2 minutes ago       Up 2 minutes                            determined_nobel
  # ceph status
    cluster 8184b724-603b-4a6f-9107-6e7417b7ee63
     health HEALTH_OK
     monmap e1: 1 mons at {centos7=192.168.178.92:6789/0}
            election epoch 3, quorum 0 centos7
      fsmap e5: 1/1/1 up {0=0=up:active}
     osdmap e15: 1 osds: 1 up, 1 in
            flags sortbitwise
      pgmap v11317: 120 pgs, 8 pools, 3704 bytes data, 32 objects
            3677 MB used, 3140 MB / 6818 MB avail
                 120 active+clean

The cluster's configuration file and key ring will automatically be available
from the directory ``/etc/ceph`` on the development system. To connect openATTIC
with the Ceph cluster, follow the `installation instructions
<http://docs.openattic.org/2.0/install_guides/post_installation.html#enabling-ceph-support-in-oa>`_
in the openATTIC manual.

Enjoy!