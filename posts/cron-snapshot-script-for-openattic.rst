.. title: Cronjob Snapshot Script for openATTIC
.. slug: cron-snapshot-script-for-openattic
.. date: 2015-12-28 13:39:41 UTC+01:00
.. tags: snapshot, python, cronjob, howto
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

This is the continuation of my last blog post :doc:`Snapshot Python script
with authtoken <snapshot-python-script-with-authtoken>`. I expanded the script
a little bit, because I don't want to create the snapshots manually every day
and I also want to store the last 10 snapshots.

.. TEASER_END 

You can change the ``maxsnaps`` variable to adjust the number of stored snapshots.

Snapshot Script::

	#!/usr/bin/env python
	import requests
	import json
	import datetime
	token = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

	headers = {'content-type': 'application/json',
	           "Authorization": "Token %s" %token}

	currentime = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')
	maxsnaps = 10

	### recorded command 1
	data=json.dumps({
		"volumeId": 615,
		"name": currentime,
		"megs": 4577
			})
	requests.post("http://openattic.yourdomain.com/openattic/api/volumes/615/snapshots", data=data, headers=headers)

	snaps = requests.get("http://openattic.yourdomain.com/openattic/api/volumes/615/snapshots?ordering=createdate", headers=headers)
	snaps = snaps.json()

	if len(snaps["results"]) > maxsnaps:
	    delete_amount = snaps["count"] - maxsnaps

	if delete_amount > 0:
	    for i in range(0, delete_amount):
	        oldest = snaps["results"][i]
	        print "DELETE oldest %s" % oldest["id"]
	        requests.delete("http://openattic.yourdomain.com/openattic/api/snapshots/%s" % oldest["id"], headers=headers)


Now you can create a normal cronjob. 

A special thanks goes to `Tatjana Dehler <link://author/tatjana-dehler>`_.
