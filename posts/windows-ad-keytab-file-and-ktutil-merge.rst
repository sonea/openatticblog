.. title: Windows AD keytab file and ktutil merge
.. slug: windows-ad-keytab-file-and-ktutil-merge
.. date: 2016-10-21 12:45:18 UTC+02:00
.. tags: windows ad, keytab, kerberos, ktuil, ktpass 
.. category: 
.. link: 
.. description: 
.. type: text
.. author: Kai Wagner

If you ever plan to setup a clustered samba fileserver within a windows active directory infastructure you'll need the following things. 

The problem in a clustered samba environment is, that the clients always wants to connect their network share with the same hostname/machine account. 

It would be possible to just use the cluster ip instead of a new machine account, but then your users/clients will always get that popup within their office programs, that this isn't a trusted location.

To get rid of that annoying problem you have to create a new machine account and merge that keytab into your existing one on your samba servers.

.. TEASER_END

1. Create a new machine account in your active directory

2. Change the password of your machine account via netdom or use the default one. When you create a computer account, the initial password is always "<computername>$".

3. Create a keytab file. Enter this command on your active directory server (this is just an example, so please change the hostname and domain first)::

	ktpass -princ host/hostname.local.lan@LOCAL.LAN -pass computername$ -out C:\computername.keytab -mapOp set -crypto RC4-HMAC-NT -p type KRB5_NT_PRINCIPAL

4. Copy the keypass to your samba servers.

5. Use ktuil on your samba servers to merge your existing and the new keytab together::

	ktutil:
	ktutil: read_kt keytab-number1
	ktutil: read_kt keytab-number2
 	ktutil: write_kt krb5.keytab
	ktutil: quit

6. Verfiy the merge::

	klist -k krb5.keytab


Thats it. Now you can use \\hostname\sharename on your windows clients to connect to your share and there shouldn't be a trust issue anymore. 
