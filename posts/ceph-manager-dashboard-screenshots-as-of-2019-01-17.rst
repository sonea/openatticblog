.. title: Ceph Manager Dashboard Screenshots as of 2019-01-17
.. slug: ceph-manager-dashboard-screenshots-as-of-2019-01-17
.. date: 2019-01-17 17:32:21 UTC+01:00
.. tags: ceph, community, dashboard, development, news, mgr
.. category: 
.. link: 
.. description: Sharing some screen shots of the Ceph Manager Dashboard for the upcoming Nautilus release
.. type: text
.. author: Lenz Grimmer

A lot has happened since I :doc:`last wrote <ceph-dashboard-v2-update>` about
the Ceph Manager Dashboard on this blog. This project has grown significantly in
several ways since then: lots of new features were added in preparation for the
upcoming Ceph "Nautilus" release, and the team working on this project has also
grown quite noticeably.

I'll be talking about this journey in more detail at the upcoming
:doc:`DevConf.CZ and FOSDEM conferences
<talking-about-ceph-manager-dashboard-at-devconfcz-and-fosdem-2019>` and
upcoming blog posts, but I'd like to share some screenshots as teasers for the
time being - enjoy!

Note that these are made from a current development snapshot, the final version
will likely look slightly different in some places. These screenshots also don't
show the embedded Grafana dashboards and have been taken on a single node
development system, so they aren't **that** impressive.

.. slides::

    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_006.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_007.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_008.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_009.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_010.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_011.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_012.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_013.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_014.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_015.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_016.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_017.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_018.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_019.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_020.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_021.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_022.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_023.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_024.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_025.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_026.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_027.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_028.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_029.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_030.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_031.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_032.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_033.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_034.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_035.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_036.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_037.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_038.png
    /galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/Ceph_Manager_Dashboard_039.png

(See the `gallery </galleries/Ceph-Mgr-Dashboard-Screens-2019-01-17/>`_ for
higher resolution images)
