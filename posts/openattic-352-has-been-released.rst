.. title: openATTIC 3.5.2 has been released
.. slug: openattic-352-has-been-released
.. date: 2017-10-10 15:26:30 UTC+02:00
.. tags: announcement, ceph, collaboration, contributing, news, opensource, release
.. category: 
.. link: 
.. description: Announcing the openATTIC 3.5.2 release
.. type: text
.. author: Sebastian Krah

We are proud to announce Version 3.5.2 of openATTIC.

Today, we broke our regular release cycle, with a good reason. After we published version 3.5.1,
we found a hand full of bugs that, from our point of view, could frustrate the user while
managing their cluster with openATTIC.

For example, in this version we prevent the user from deleting buckets that are referenced/used by NFS etc.

Furthermore, due to changes in the latest Ceph version, it wasn't possible to create
erasure code profiles anymore. This issue has been fixed in this release as well. 

Some other minor changes and UI improvements made it also into this release, see the CHANGELOG for further details.

.. TEASER_END

We would like to thank everyone who contributed to this release.
Your feedback, ideas and bug reports are very welcome.
If you would like to get in touch with us, consider either joining our `openATTIC Users`_ Google Group,
visiting our `#openattic`_ channel on `irc.freenode.net`_ or leaving comments below this blog post.
See the list below for a more detailed changelog and further references.
The OP codes in brackets refer to individual Jira issues that provide additional details on each item.
You can review these on our `public JIRA instance`_.

Changelog for version 3.5.2
---------------------------

Added
~~~~~
* WebUI: Do not delete buckets that are referenced/used, e.g. by NFS (:issue:`OP-2536`)

Changed
~~~~~~~
* WebUI: RBD features: Add more information to features that
  require another feature to be enabled (:issue:`OP-2765`)
* WebUI: Add separator between action buttons in Ceph OSD page (:issue:`OP-2748`)

Fixed
~~~~~
* Backend: Fixed Erasure Code Profile creation (:issue:`OP-2758`)
* Backend: Hide the "machine" drop-down on the Nodes tab (:issue:`OP-2510`)
* Backend: Fixed `module_status/ceph_deployment`: behave exactly as
  `module_status/ceph` (:issue:`OP-2720`)
* WebUI/QA: Fix random ceph pool creation e2e test (:issue:`OP-2762`)
* WebUI: Fix error when creating RBD without size (:issue:`OP-2756`)

.. _openATTIC Users: https://groups.google.com/forum/#%21forum/openattic-users
.. _#openattic: irc://irc.freenode.org/#openattic
.. _irc.freenode.net: http://webchat.freenode.net/?channels=openattic
.. _public JIRA instance: http://tracker.openattic.org/
