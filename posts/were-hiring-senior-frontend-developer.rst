.. title: We're hiring: Senior Frontend Developer
.. slug: were-hiring-senior-frontend-developer
.. date: 2016-12-05 15:26:28 UTC+01:00
.. tags: opensource, development, collaboration, suse, hiring
.. category:
.. link:
.. description: Broadcasting the newly opened UI developer role
.. type: text
.. author: Lenz Grimmer

Now that we've :doc:`joined SUSE <openattic-joins-suse>`, we're able to extend
the team working on openATTIC!

We've just opened a new position and are now hiring for the position of a
"`Senior Frontend Developer Enterprise Storage Management
<https://attachmatehr.silkroad.com/epostings/index.cfm?fuseaction=app.jobinfo&jobid=308872&company_id=15495&version=6&source=ONLINE&JobOwner=1014654&startflag=1>`_".

In this role, you'll be working with the openATTIC team on adding new features
to openATTIC's web-based management frontend, as well as improving and extending
existing functionality.

The openATTIC web interface is based on well-known web development technologies
like AngularJS and Bootstrap and communicates with the openATTIC backend via
it's REST API.

See the job opening for further details on our expectations and requirements. If
you have any questions or would like to learn more, don't hesitate to `get in
touch <http://openattic.org/get-involved.html>`_ with us!

By the way, if you're interested in working on open source software, SUSE
currently has `70+ job offerings <https://www.suse.com/de-de/company/careers/>`_
available!

**Update 2017-02-17:** this position has been filled in the meanwhile (welcome
Ricardo Marques to our team!)

However we're :doc:`now hiring <were-hiring-senior-backend-developer>` for a
senior backend developer role!