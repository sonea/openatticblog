.. title: openATTIC 3.5.0 has been released
.. slug: openattic-350-has-been-released
.. date: 2017-09-15 09:54:34 UTC
.. tags: announcement, ceph, collaboration, contributing, news, beta, opensource, release
.. category:
.. link:
.. description: Announcing the openATTIC 3.5.0 release
.. type: text
.. author: Laura Paduano

We are happy to announce version 3.5.0 of openATTIC.
With this release we continued integrating Ceph Luminous features.
One of those features is the possibility to edit Ceph pools - the size of PGs
can be changed and pool applications can be enabled or disabled.
Furthermore we integrated the functionality to manage OSD properties
(for example "no out") cluster-wide.
The Grafana dashboard got an update as well: we added more statistics for
Rados Gateway users and buckets (`contribution to DeepSea`_).

When using the "Striping"-feature for an RBD there will be two more
fields in the UI "striping unit" and "striping count" which can be set.
This feature also comes with helpful information for the user while editing or
changing values.

As usual we improved the usability of our UI and fixed some minor bugs.
Also our documentation now contains a `troubleshooting guide`_.

.. TEASER_END

We would like to thank everyone who contributed to this release.
Your feedback, ideas and bug reports are very welcome.
If you would like to get in touch with us, consider joining our openATTIC Users Google Group,
visit our #openattic channel on irc.freenode.net or leave comments below this blog post.
See the list below for a more detailed change log and further references.
The OP codes in brackets refer to individual Jira issues that provide additional details on each item.
You can review these on our public JIRA instance.

Changelog for version 3.5.0
---------------------------

Added
~~~~~
* Documentation: Added Troubleshooting chapter (:issue:`OP-1070`)
* Backend: Added `osd_flags` to `CephCluster` (:issue:`OP-2598`)
* QA/Development: Export HTML e2e report (:issue:`OP-2261`)
* WebUI: Add Ceph cluster settings modal (:issue:`OP-2599`)
* WebUI: iSCSI Target dialog should support RBDs with data-pool
  enabled (:issue:`OP-2633`)
* WebUI: Add Feedback panel to the UI (:issue:`OP-100`)
* WebUI: Ceph pool `Placement Groups` and `Applications` edition (:issue:`OP-2608`)
* WebUI: Enable the RBD striping-v2 feature (:issue:`OP-2454`)

Changed
~~~~~~~
* Packaging: Depend on Ceph Luminous (:issue:`OP-2622`)
* WebUI: Upgrade Protractor (:issue:`OP-2618`)
* Documentation: Changed the project descriptions in `README.rst` and
  `openattic.spec` to reflect the wording used on openattic.org (:issue:`OP-2643`)
* WebUI/QA: Improve protractor report configuration (:issue:`OP-2647`)
* WebUI/QA: Improve the e2e framework to execute tests in a Vagrant box
  environment (:issue:`OP-2640`)
* Documentation: Update e2e documentation (:issue:`OP-2329`)
* WebUI: Replace JSCS and JSHint with ESLint (:issue:`OP-2597`)
* WebUI: Hide RBD usage percentage bar if no usage information is
  available (:issue:`OP-2639`)
* WebUI: Fix 'Configure Cluster-wide OSD Flags' header button
  position (:issue:`OP-2657`)
* WebUI: Display an error message when the loading of the form data
  fails (:issue:`OP-2656`)
* Installation: Drop database when calling `oaconfig install` without data
  loss (:issue:`OP-2577`)

Fixed
~~~~~
* Backend: Stop `requests.packages.urllib3` from spamming the log
  file (:issue:`OP-2616`)
* WebUI/QA: Fix random e2e failure - ceph rgw buckets should delete the
  test user (:issue:`OP-2627`)
* Fixed Grafana Node Statistics are not immediately visible in oA (:issue:`OP-2634`)
* Backend: Fixed `KeyError: 'hostname'` in nodes tab (:issue:`OP-2638`)
* Backend: Fixed deleting Pools with Luminous (:issue:`OP-2216`)
* WebUI/QA: Fix random NFS e2e timeout failure (:issue:`OP-2641`)
* WebUI: Fix dashboard error if no user profile exists (:issue:`OP-2649`)
* WebUI: The statistic/detail tab page is still displayed when the
  datatable is reloaded (:issue:`OP-2537`)
* Installation: Fixed database migration `ifconfig.0005` (:issue:`OP-2646`)
* WebUI: MON widget will verify if timechecks are there (:issue:`OP-2655`)
* WebUI/QA: Harden task queue directive test cases (:issue:`OP-2619`)
* WebUI/QA: Fix random e2e test failure (:issue:`OP-2621`)
* Backend: Internal Server Error when creating a new NFS export with
  "Object Gateway" as type. (:issue:`OP-2652`)

.. _openATTIC Users: https://groups.google.com/forum/#%21forum/openattic-users
.. _#openattic: irc://irc.freenode.org/#openattic
.. _irc.freenode.net: http://webchat.freenode.net/?channels=openattic
.. _public JIRA instance: http://tracker.openattic.org/
.. _contribution to DeepSea: https://github.com/SUSE/DeepSea/pull/626
.. _troubleshooting guide: http://docs.openattic.org/en/latest/install_guides/troubleshooting.html
