.. title: openATTIC 2.0.18 beta has been released
.. slug: openattic-2018-beta-has-been-released
.. date: 2017-02-10 10:04:32 UTC+01:00
.. tags: announcement, collaboration, contributing, news, beta, opensource, release
.. category: 
.. link: 
.. description: Announcing the 2.0.18 beta release
.. type: text
.. author: Tatjana Dehler

We are very happy to announce the first openATTIC release in 2017. Although it is not as extensive
as usual it contains some significant highlights:

This release comes with a very new feature - the implementation of a Taskqueue module.
Tasks that take more time, for example the creation of placement groups in a Ceph cluster, can
now be controlled in the openATTIC GUI. The Taskqueue adds the functionality to create tasks,
run them in the background, track their status and collect their results. We also added some
sections about background tasks and the Taskqueue usage to our documentation.

Furthermore, we've made some usability improvements. The installed version of all known openATTIC
hosts is displayed in the GUI now. Buttons and pages without any function have been removed from
the user interface.

.. TEASER_END

In preparation of the update from angular 1.x to angular 2.x we restructured various parts of the
GUI. Nearly every feature is now stored in its own module folder. In addition, we standardised the
naming of the names of the frontend files.

We've also added RBD's utilization graphs to the Ceph GUI.

To download and install openATTIC, please follow the `installation instructions
<http://docs.openattic.org/2.0/install_guides/index.html>`_ in the
documentation.

Please note that 2.0.18 is still a **beta version**. As usual, handle with care
and make sure to create backups of your data!

We would like to thank everyone who contributed to this release, especially Ricardo
Marques and Ricardo Dias.

Your feedback, ideas and bug reports are very welcome. If you would like to get
in touch with us, consider joining our `openATTIC Users
<https://groups.google.com/forum/#%21forum/openattic-users>`_ Google Group,
visit our `#openattic <irc://irc.freenode.org/#openattic>`_ channel on
`irc.freenode.net <http://webchat.freenode.net/?channels=openattic>`_ or leave
comments below this blog post.

See the list below for a more detailed change log and further references. The OP
codes in brackets refer to individual Jira issues that provide additional
details on each item. You can review these on our `public Jira instance
<http://tracker.openattic.org/>`_.

Changelog for 2.0.18:

* WebUI: Enables the RBD form to create RBDs after reloading (:issue:`OP-1839`)
* WebUI: Enabled multiple host deletion (:issue:`OP-1816`)
* WebUI: Improve RBD creation error messages (:issue:`OP-1527`)
* WebUI: Fixes WWN duplication error (:issue:`OP-1842`)
* WebUI: Task-queue UI implementation (:issue:`OP-1418`)
* WebUI: Fixes loading spinner disappearance problem (:issue:`OP-1495`)
* WebUI: Displays RBD utilization graphs in the RBD details view (:issue:`OP-1592`)
* WebUI: Show the openATTIC version number on each node (:issue:`OP-1619`)
* WebUI: Removed not implemented widgets (:issue:`OP-1191`)
* WebUI: Colorize the disk/pool/volume status icons depending on their
  classification. (:issue:`OP-1891`)
* WebUI/QA: Tests for task-queue implementation (:issue:`OP-1746`)
* WebUI/QA: Added test that deletes multiple hosts at once (:issue:`OP-1817`)
* Backend/QA: Added timestamps to verbose output to map the testcase and to
  the related ``openattic.log`` file entry (:issue:`OP-1841`)
* Deployment: Improve error messages and handling of errors of ``make_dist.py``
  related to debchange (:issue:`OP-1804`)
* Installation: Changing the database password during ``oaconfig install`` now
  follows the symlink from ``database.ini`` to ``pgsql.ini`` instead of creating a new
  file (:issue:`OP-1821`)
* Installation: Fixed %post scriplet failure in the ``openattic-module-ceph`` RPM
  package (:issue:`OP-1820`)
* Installation: Added missing configuration entries to the SUSE sysconfig
  file (PR#576)
* Installation: Make ``oaconfig`` respect the username and database name stored in
  ``database.ini`` to create/connect to a database (PR#599)
* Packaging: Added the ability to add mercurial tags to a repository when
  creating a tarball using ``make_dist.py`` (:issue:`OP-1374`)
* Packaging: Enable to use suffixes for the tarball creation using
  ``make_dist.py`` (:issue:`OP-1875`)
* Packaging: Confusing message about how to delete the openATTIC database is
  no longer shown when updating the ``openattic-pgsql`` package (:issue:`OP-1537`)
* Development: Fix ``rtslib`` import warnings in Vagrant Xenial boxes. (:issue:`OP-1858`)
* Development: Reconfigure default gateway in Vagrant boxes to set the correct
  primary interface during ``oaconfig install``. (:issue:`OP-1874`)
* Documentation: Adapt Vagrant developer documentation.
* Documentation: Documentation of background task and task-queue
  usage (:issue:`OP-1747`)
* Documentation: Added Ubuntu Xenial to the list of distributions provided by
  our repository (:issue:`OP-1543`)