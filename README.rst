openATTIC Web Site and Blog
===========================

This repository contains the source code of the openATTIC web site and blog
which are static web sites hosted on https://blog.openattic.org/ and
https://www.openattic.org respectively.

Content is created in the `reStructuredText
<http://docutils.sourceforge.net/rst.html>`_ markup syntax and converted into
HTML pages using `Nikola <https://getnikola.com/>`_, a static web site generator
written in Python.

Project Structure
-----------------

The top level configuration file is ``conf.py``. It is well commented, please
refer to the `Nikola Documentation <https://getnikola.com/documentation.html>`_
for more details.

The content in the repository is organized in subdirectories as follows:

``files``: Static assets like images, CSS that define the overall look and feel
of the site.

``galleries``: Collections of pictures to be displayed in the form of a slide
show (e.g. on a page or post).

``images``: Individual images and logos to be included in pages or posts.

``pages``: Static web pages (in reStructuredText format).

``plugins``: Custom Nikola plugins used for rendering the content.

``posts``: The directory containing individual blog posts (in reStructuredText
format).

Creating content on this site
-----------------------------

If you want to contribute content, you need to have a user account on the
BitBucket web site and should be logged in. If you don't have one yet, head over
to the `Create your account <https://bitbucket.org/account/signup/>`_ page to
get started.

You also need to set up a local development environment based on the following
tools:

- The `git <https://www.git-scm.com/>`_ distributed version control system, to
  check out a local copy of the blog's source code.
- A local instance of Nikola installed on your development system. Simply follow
  the `getting started instructions <https://getnikola.com/getting-started.html>`_
  on the Nikola web site.
- A text editor of your choice, preferrably one that has support for the
  reStructuredText markup language.
- A web browser to preview your changes.

Start by `forking the repository
<https://bitbucket.org/openattic/openatticblog/fork>`_ and then perform a local
clone of the forked repository::

  $ git clone git@bitbucket.org:<username>/openatticblog.git
  $ cd openatticblog

See the `BitBucket documentation on forking
<https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html>`_
for more details.

To keep your repository in sync with the upstream repository, you should add it
as an additional remote repository to git as follows::

  $ git remote add upstream git@bitbucket.org:openattic/openatticblog.git

Now you can pull in changes from the upstream repository via ``git fetch
upstream``.

Creating a virtual environment for nikola
-----------------------------------------

To avoid polluting your development system you should create and use an isolated
Python environment to install and use nikola.

To setup the virtual Python environment execute the following lines::

  $ python3 -m venv venv
  $ source venv/bin/activate
  $ pip install -r requirements.txt

If the virtual environment already exists, then remember to keep the packages
up to date.

Now you can execute nikola within this virtual environment to create or modify
blog posts.

Creating a blog post
--------------------

To create a new article, make sure you are in the ``openatticblog`` directory.
Then use the ``nikola`` command line tool to generate a stub posting::

  $ nikola new_post -a "Your Name" -t "Title of the Blog Post"

Nikola will inform you where the article template can be found. Open the file in
your favorite text editor and get creative! Make sure to fill out the fields
``tags:`` and ``description`` with useful information (take a look at some
existing posts for inspiration).

If you want to generate a preview of your post, the command ``nikola auto``
comes in handy - it rebuilds the site every time you save changes made to your
file (you will need to install some additional python libraries ``pip install watchdog ws4py``).
By default, the web site can be previewed in your web browser via the URL http://localhost:8000/

Take a look at the `Nikola Handbook <https://getnikola.com/handbook.html>`_ for
more information on how to create content.

Once you're done, commit your changes locally and push them into your fork on
BitBucket::

  $ git add posts/<title-of-your-blog-post.rst>
  $ git commit -s -m "New Post: Title of the Blog Post"
  $ git push origin

Next, `submit a pull request
<https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html>`_
as outlined in the BitBucket documentation.

After review, your pull request will be merged into the upstream repository,
which will trigger an automatic rebuild of the web site (via a Jenkins job).
